<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl4">
        <span>リンク集</span>
      </h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-news--detail-inner">
      <div class="p-news--detail-cnt">
        <div class="p-guide--update p-guide--link">
          <div class="p-guide--update-head">
            <ul class="p-guide--anchor">
              <li>
                <a href="#government"><span>国・自治体</span></a>
              </li>
              <li class="active">
                <a href="#tokyo"><span>東京都関連施設・団体等</span></a>
              </li>
              <li>
                <a href="#organizations"><span>芸術文化関係団体他</span></a>
              </li>
            </ul>
          </div><!-- ./p-guide--update-head -->
          <div class="p-guide--update-cnt">
            <div class="p-guide--update-row" id="government">
              <h4 class="section-title-ep">国・自治体</h4>
              <div class="p-guide--update-row-inner">
                <ul class="p-guide--link-row-list">
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">文化庁</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都生活文化局《文化振興》</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">「アートにエールを！東京プロジェクト」</a></li>
                </ul>
              </div>
            </div><!-- ./p-guide--update-row -->
            <div class="p-guide--update-row" id="tokyo">
              <h4 class="section-title-ep">東京都関連施設・団体等</h4>
              <div class="p-guide--update-row-inner">
                <ul class="p-guide--link-row-list">
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都庭園美術館</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都江戸東京博物館</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">江戸東京たてもの園</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都写真美術館</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都現代美術館</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都渋谷公園通りギャラリー</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">トーキョーアーツアンドスペース</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京都美術館</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京文化会館</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京芸術劇場</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京舞台芸術活動支援センター（水天宮ピット）</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">Tokyo Museum Collection:東京都立博物館・美術館収蔵品検索</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">都民芸術フェスティバル</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">公益財団法人東京都歴史文化財団</a></li>
                </ul>
              </div>
            </div><!-- ./p-guide--update-row -->
            <div class="p-guide--update-row" id="organizations">
              <h4 class="section-title-ep">芸術文化関係団体他</h4>
              <div class="p-guide--update-row-inner">
                <ul class="p-guide--link-row-list">
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">独立行政法人日本芸術文化振興会</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">一般社団法人地域創造</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">公益財団法人全国公立文化施設協会</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">公益財団法人日本芸能実演家団体協議会</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">東京ミュージアム・ぐるっとパス</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">公益財団法人東京都交響楽団</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">公益財団法人メセナ協議会</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">アートマネジメント総合情報サイト:ネットTAM</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">登竜門</a></li>
                  <li><a href="" target="_blank" class="link-icon2 blank-blue">GO TOKYO 　東京の観光公式サイト</a></li>
                </ul>
              </div>
            </div><!-- ./p-guide--update-row -->
          </div><!-- ./p-guide--update-cnt -->
        </div><!-- ./p-guide--update -->
      </div>
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2">
    <a href="/guide" class="view-more2"><span>トップページへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>リンク集</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>