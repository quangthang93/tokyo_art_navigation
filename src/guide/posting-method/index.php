<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl4">
        GUIDE
        <span>投稿について</span>
      </h1>
      <p class="ttl-line-green2 mgt-15"><a href="">展覧会・イベント情報</a></p>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-guide--nav">
      <ul class="p-guide--nav-list">
        <li class="active">
          <a href="/guide/posting-method"><span>このサイトについて</span></a>
        </li>
        <li>
          <a href="/guide/update-method"><span>更新方法について</span></a>
        </li>
        <li>
          <a href="/guide/faq"><span>よくあるご質問</span></a>
        </li>
      </ul>
    </div>
    <div class="p-news--detail-inner">
      <div class="p-news--detail-cnt">
        <div class="p-guide--posting">
          <div class="p-guide--posting-head">
            <p class="desc4">Tokyo Art Navigationでは、東京都内・近郊で開催中・開催予定の「展覧会・イベント情報」の告知をすることができます。</p>
            <p class="notice">※公式ウェブサイトまたは案内ページへのリンクが前提となります。</p>
            <div class="p-guide--posting-head-list">
              <ul class="p-insta--cnt-notices-item-subList">
                <li>新進アーティストの「展覧会・イベント情報」を中心に情報を掲載していますが、それ以外のものも受け付けます。</li>
                <li>ジャンルは問いませんが、情報掲載の可否・掲載時期は運営事務局で判断いたします。（掲載可否の理由は開示できません。予めご了承ください。）</li>
              </ul>
            </div>
          </div><!-- ./p-guide--posting-head -->
          <div class="p-guide--posting-cnt">
            <div class="p-guide--posting-row">
              <h4 class="section-title-ep">公開までの流れ</h4>
              <div class="p-guide--posting-row-inner">
                <ul class="p-guide--posting-row-list">
                  <li class="p-guide--posting-row-item">
                    <div class="p-guide--posting-row-item-num">
                      <span class="_label">STEP</span>
                      <span class="_num">1</span>
                    </div>
                    <div class="p-guide--posting-row-item-infor">
                      <p class="title-lv4">アカウント作成</p>
                      <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </li>
                  <li class="p-guide--posting-row-item">
                    <div class="p-guide--posting-row-item-num">
                      <span class="_label">STEP</span>
                      <span class="_num">2</span>
                    </div>
                    <div class="p-guide--posting-row-item-infor">
                      <p class="title-lv4">開催情報登録</p>
                      <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </li>
                  <li class="p-guide--posting-row-item">
                    <div class="p-guide--posting-row-item-num">
                      <span class="_label">STEP</span>
                      <span class="_num">3</span>
                    </div>
                    <div class="p-guide--posting-row-item-infor">
                      <p class="title-lv4">公開依頼</p>
                      <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </li>
                  <li class="p-guide--posting-row-item">
                    <div class="p-guide--posting-row-item-num">
                      <span class="_label">STEP</span>
                      <span class="_num">4</span>
                    </div>
                    <div class="p-guide--posting-row-item-infor">
                      <p class="title-lv4">運営事務局確認</p>
                      <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </li>
                  <li class="p-guide--posting-row-item">
                    <div class="p-guide--posting-row-item-num">
                      <span class="_label">STEP</span>
                      <span class="_num">5</span>
                    </div>
                    <div class="p-guide--posting-row-item-infor">
                      <p class="title-lv4">サイト公開</p>
                      <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </li>
                </ul>
                <div class="align-center">
                  <a href="" class="btn-blank" target="_blank">アカウント作成はこちら</a>
                </div>
              </div>
            </div><!-- ./p-guide--posting-row -->
            <div class="p-guide--posting-row">
              <h4 class="section-title-ep">ご利用条件</h4>
              <div class="p-guide--posting-row-inner">
                <ul class="p-insta--cnt-notices-item-subList">
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                </ul>
              </div>
            </div><!-- ./p-guide--posting-row -->
            <div class="p-guide--posting-row">
              <h4 class="section-title-ep">ご注意事項</h4>
              <div class="p-guide--posting-row-inner">
                <ul class="p-insta--cnt-notices-item-subList">
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                  <li>この文章はダミーコピーですお読みにならないで下さい。</li>
                </ul>
                <div class="p-guide--posting-row-link">
                  <div>
                    <a href="" class="link-icon type4">ソーシャルメディア運用方針</a>
                  </div>
                  <div>
                    <a href="" class="link-icon type4">プライバシーポリシー</a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-guide--posting-row -->
          </div><!-- ./p-guide--posting-cnt -->
        </div><!-- ./p-guide--posting -->
      </div>
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2">
    <a href="/guide" class="view-more2"><span>ご利用方法トップへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>ご利用方法</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>