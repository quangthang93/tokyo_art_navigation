<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <h1 class="p-end--ttl">
      <span class="big">GUIDE</span>
      <span class="small">Tokyo Art Navigationは、自分の作品を世界に届けたいアーティストと<br class="pc-only3">感性に響くアートを見つけたいあなたをつなげます。</span>
    </h1>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-guide--nav">
      <ul class="p-guide--nav-list">
        <li>
          <a href="/guide"><span>このサイトについて</span></a>
        </li>
        <li class="active">
          <a href="/guide/introduction"><span>投稿コンテンツ紹介</span></a>
        </li>
      </ul>
    </div>
    <div class="p-news--detail-inner">
      <div class="p-news--detail-cnt">
        <div class="p-guide--about">
          <div class="p-guide--introduction-list">
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">TOKYO ARTISTS</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">展覧会・イベント情報</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">コンテスト情報</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">支援情報</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="p-insta--cnt-notices p-guide--notices">
    <div class="container6">
      <div class="p-end--ttlIntroWrap">
        <p class="p-end--ttlIntro" href=""><span>ご注意事項</span></p>
      </div>
      <div class="p-insta--cnt-notices-list">
        <div class="p-insta--cnt-notices-item">
          <p class="p-insta--cnt-notices-item-ttl">●掲載に当たっては、運営事務局の掲載基準に基づき、掲載の可否を判断しております。掲載可否の内容は以下の通りです。 ※掲載可否基準は、順次変更していく場合があります。詳しくは「ご利用規約」をご確認ください。</p>
          <ul class="p-insta--cnt-notices-item-subList">
            <li>公序良俗に反するもの、その恐れがあるもの</li>
            <li>アダルト関連のもの、またはその恐れがあるもの</li>
            <li>犯罪行為を誘発する恐れのあるもの</li>
            <li>青少年に悪影響を及ぼす恐れがあるもの</li>
            <li>第三者の著作権・商標権等の知的財産を侵害している内容、またその恐れがあるもの</li>
            <li>第三者の名誉・信用・プライバシー・肖像権等の人格的権利を侵害している内容、若しくはその恐れがあるもの。差別をするもの若しくはそれを助長するもの。</li>
            <li>法律・法規・条例等に反する広告、またはその恐れがあるもの</li>
            <li>反社会的なもの</li>
            <li>虚偽・誇大ならびに事実誤認を生じさせるものがある内容</li>
            <li>その他、弊社確認により掲載不可と判断したもの</li>
          </ul>
        </div>
        <div class="p-insta--cnt-notices-item">
          <p class="p-insta--cnt-notices-item-ttl">●掲載後におきましても、ユーザークレームの発生、またはその危惧があると判断したものは、掲載途中であっても、掲載を停止する場合がございますのでご了承ください。（その理由説明の義務は負わないものとさせていただきます）</p>
        </div>
        <div class="p-insta--cnt-notices-item">
          <p class="p-insta--cnt-notices-item-ttl">●掲載にあたっては、アーティストとしての活動歴について、公表されている方に限らせていただきます。</p>
        </div>
        <div class="p-insta--cnt-notices-item">
          <p class="p-insta--cnt-notices-item-ttl">●掲載にあたって、ご投稿内容の確認を行い、承認された投稿のみを掲載しております。</p>
        </div>
        <div class="p-insta--cnt-notices-item">
          <p class="p-insta--cnt-notices-item-ttl">●ご投稿から掲載までにお時間がかかる場合がございますので、予めご了承ください。</p>
        </div>
        <div class="p-insta--cnt-notices-item">
          <p class="p-insta--cnt-notices-item-ttl">●掲載するご投稿は1人1点までとさせていただきます。</p>
        </div>
      </div>
      <div class="p-guide--notices-link">
        <div><a href="" class="link-icon type4">ソーシャルメディア運用方針</a></div>
        <div><a href="" class="link-icon type4">プライバシーポリシー</a></div>
      </div>
    </div>
  </div>
  <div class="align-center fadeup delay-2">
    <a href="/" class="view-more2"><span>トップページへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>ご利用方法(投稿コンテンツ紹介)</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>