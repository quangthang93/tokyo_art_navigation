<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl4">
        GUIDE
        <span>投稿について</span>
      </h1>
      <p class="ttl-line-green2 mgt-15"><a href="">展覧会・イベント情報</a></p>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-guide--nav">
      <ul class="p-guide--nav-list">
        <li>
          <a href="/guide/posting-method"><span>このサイトについて</span></a>
        </li>
        <li>
          <a href="/guide/update-method"><span>更新方法について</span></a>
        </li>
        <li class="active">
          <a href="/guide/faq"><span>よくあるご質問</span></a>
        </li>
      </ul>
    </div>
    <div class="p-news--detail-inner">
      <div class="p-news--detail-cnt">
        <div class="p-guide--faq">
          <ul class="p-guide--faq-list">
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">誰でも「展覧会・イベント情報」を掲載できますか？</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">「展覧会・イベント情報」で情報を掲載できない場合を教えてください。</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">「展覧会・イベント情報」の申請から情報公開まで、どれぐらいの時間がかかりますか？</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">「展覧会・イベント情報」の開催期間が変更になりました。修正方法を教えてください。</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">「展覧会・イベント情報」の会期終了後、情報が非表示になりました。どうしてですか？</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">「展覧会・イベント情報」の掲載情報を削除したいのですが、どうすればよいですか？</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">「展覧会・イベント情報」の公式ウェブサイト・告知ページがない場合は、どうすればよいですか？</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
            <li class="p-guide--faq-item">
              <p class="p-guide--faq-item-qa ttl-bold question">主催者・関係者ではない第三者により、意図しない「展覧会・イベント情報」が掲載されています。削除できますか？</p>
              <p class="p-guide--faq-item-qa desc answer">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
            </li>
          </ul><!-- ./p-guide--faq-list -->
        </div><!-- ./p-guide--update -->
      </div>
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2">
    <a href="/guide" class="view-more2"><span>ご利用方法トップへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>ご利用方法</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>