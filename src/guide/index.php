<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <h1 class="p-end--ttl">
      <span class="big">GUIDE</span>
      <span class="small">Tokyo Art Navigationは、自分の作品を世界に届けたいアーティストと<br class="pc-only3">感性に響くアートを見つけたいあなたをつなげます。</span>
    </h1>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-guide--nav">
      <ul class="p-guide--nav-list">
        <li class="js-tabsItem active">
          <a href="javascript:void(0)"><span>このサイトについて</span></a>
        </li>
        <li class="js-tabsItem">
          <a href="javascript:void(0)"><span>投稿コンテンツ紹介</span></a>
        </li>
      </ul>
    </div>
    <div class="p-news--detail-inner">
      <div class="p-news--detail-cnt js-tabsCnt">
        <div class="p-guide--about js-tabsPanel">
          <div class="p-guide--about-list">
            <div class="p-guide--about-item">
              <div class="p-guide--about-item-inner js-accorCntWrap">
                <div class="p-guide--about-item-head">
                  <div class="p-guide--about-item-thumbWrap">
                    <div class="p-guide--about-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/guide/guide01.png" alt="">
                    </div>
                    <div class="p-guide--about-item-ttlWrap">
                      <p class="p-guide--about-item-subTtl">アート情報を知りたい！</p>
                      <h3 class="p-guide--about-item-ttl _ttl">一般ユーザー</h3>
                    </div>
                  </div>
                  <div class="p-guide--about-item-intro">
                    東京のアートシーンをナビゲート！展覧会・イベントや都内を中心に活動するアーティスト情報、充実のコラムが満載です。あなただけのアーティストを発見しましょう！
                  </div>
                </div>
                <div class="p-guide--about-item-main js-accorCnt">
                  <h4 class="section-title-ep">コンテンツ紹介</h4>
                  <div class="p-guide--about-item-main-cnt">
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl en">COLUMN</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">展覧会・イベント情報</a>
                          <span class="tag2">投稿可</span>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl en">TOKYO ARTISTS</a>
                          <span class="tag2">投稿可</span>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">コンテスト情報</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">アーティスト支援制度</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="p-guide--about-item-ctrl _ctrl">
                <a href="javascript:void(0)" class="btn-guideCtrl js-btnGuideCtrl js-accorLabel">コンテンツ紹介</a>
              </div>
            </div><!-- ./p-guide--about-item -->
            <div class="p-guide--about-item green">
              <div class="p-guide--about-item-inner js-accorCntWrap">
                <div class="p-guide--about-item-head">
                  <div class="p-guide--about-item-thumbWrap">
                    <div class="p-guide--about-item-thumb type2">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/guide/guide02.png" alt="">
                    </div>
                    <div class="p-guide--about-item-ttlWrap">
                      <p class="p-guide--about-item-subTtl">アート活動を発信したい！</p>
                      <h3 class="p-guide--about-item-ttl _ttl">アーティスト</h3>
                    </div>
                  </div>
                  <div class="p-guide--about-item-intro">
                    東京のアートシーンをナビゲート！展覧会・イベントや都内を中心に活動するアーティスト情報、充実のコラムが満載です。あなただけのアーティストを発見しましょう！
                  </div>
                </div>
                <div class="p-guide--about-item-main js-accorCnt">
                  <h4 class="section-title-ep">コンテンツ紹介</h4>
                  <div class="p-guide--about-item-main-cnt">
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl en">COLUMN</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">展覧会・イベント情報</a>
                          <span class="tag2">投稿可</span>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl en">TOKYO ARTISTS</a>
                          <span class="tag2">投稿可</span>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">コンテスト情報</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">アーティスト支援制度</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="p-guide--about-item-ctrl _ctrl">
                <a href="javascript:void(0)" class="btn-guideCtrl green js-btnGuideCtrl js-accorLabel">コンテンツ紹介</a>
              </div>
            </div><!-- ./p-guide--about-item -->
            <div class="p-guide--about-item red">
              <div class="p-guide--about-item-inner js-accorCntWrap">
                <div class="p-guide--about-item-head">
                  <div class="p-guide--about-item-thumbWrap">
                    <div class="p-guide--about-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/guide/guide03.png" alt="">
                    </div>
                    <div class="p-guide--about-item-ttlWrap">
                      <p class="p-guide--about-item-subTtl">アーティストを支えたい！</p>
                      <h3 class="p-guide--about-item-ttl _ttl">支援者</h3>
                    </div>
                  </div>
                  <div class="p-guide--about-item-intro">
                    東京のアートシーンをナビゲート！展覧会・イベントや都内を中心に活動するアーティスト情報、充実のコラムが満載です。あなただけのアーティストを発見しましょう！
                  </div>
                </div>
                <div class="p-guide--about-item-main js-accorCnt">
                  <h4 class="section-title-ep">コンテンツ紹介</h4>
                  <div class="p-guide--about-item-main-cnt">
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl en">COLUMN</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">展覧会・イベント情報</a>
                          <span class="tag2">投稿可</span>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl en">TOKYO ARTISTS</a>
                          <span class="tag2">投稿可</span>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">コンテスト情報</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                    <div class="p-guide--about-item-main-row">
                      <div class="p-guide--about-item-main-item">
                        <div class="p-guide--about-item-main-ttlWrap">
                          <a href="" class="p-guide--about-item-main-ttl">アーティスト支援制度</a>
                        </div>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="p-guide--about-item-ctrl _ctrl">
                <a href="javascript:void(0)" class="btn-guideCtrl red js-btnGuideCtrl js-accorLabel">コンテンツ紹介</a>
              </div>
            </div><!-- ./p-guide--about-item -->
          </div><!-- ./p-guide--about-list -->
        </div>
        <div class="p-guide--about js-tabsPanel">
          <div class="p-guide--introduction-list">
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">TOKYO ARTISTS</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">展覧会・イベント情報</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">コンテスト情報</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
            <div class="p-guide--introduction-item">
              <h4 class="section-title-ep">支援情報</h4>
              <div class="p-guide--introduction-item-inner">
                <div class="p-guide--introduction-item-cnt">
                  <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。</p>
                </div>
                <div class="p-guide--introduction-item-direct">
                  <a href="" class="btn-guide"><span>投稿方法について</span></a>
                  <a href="" class="btn-guide type2"><span>更新方法について</span></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2">
    <a href="/" class="view-more2"><span>トップページへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>ご利用方法(このサイトについて)</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>