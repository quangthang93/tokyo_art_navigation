<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl4">
        GUIDE
        <span>投稿について</span>
      </h1>
      <p class="ttl-line-green2 mgt-15"><a href="">展覧会・イベント情報</a></p>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-guide--nav">
      <ul class="p-guide--nav-list">
        <li>
          <a href="/guide/posting-method"><span>このサイトについて</span></a>
        </li>
        <li class="active">
          <a href="/guide/update-method"><span>更新方法について</span></a>
        </li>
        <li>
          <a href="/guide/faq"><span>よくあるご質問</span></a>
        </li>
      </ul>
    </div>
    <div class="p-news--detail-inner">
      <div class="p-news--detail-cnt">
        <div class="p-guide--update">
          <div class="p-guide--update-head">
            <p class="ttl-bold">「展覧会・イベント情報」の更新手順をご案内します。</p>
            <div class="p-guide--update-head-direct">
              <a href="" class="btn-blank" target="_blank">アカウント作成はこちら</a>
              <a href="" class="btn-blank type2" target="_blank">アカウントを作成する</a>
            </div>
            <ul class="p-guide--anchor">
              <li>
                <a href="#create_account"><span>アカウントを作成する</span></a>
              </li>
              <li class="active">
                <a href="#update_procedure"><span>更新手順 (情報登録から公開まで)</span></a>
              </li>
              <li>
                <a href="#correction"><span>公開情報の修正・削除</span></a>
              </li>
            </ul>
          </div><!-- ./p-guide--update-head -->
          <div class="p-guide--update-cnt">
            <div class="p-guide--update-row" id="create_account">
              <h4 class="section-title-ep">アカウントを作成する</h4>
              <div class="p-guide--update-row-inner">
                <div class="p-guide--update-row-summary">
                  <p class="ttl-bold">「展覧会・イベント情報」の情報掲載は、展覧会・イベントの主催者・関係者ご自身での情報登録をお願いしております。</p>
                  <p class="notice">情報登録には、更新用のアカウントを作成いただく必要があります。</p>
                  <p class="title-lv4">ご準備いただくもの</p>
                  <p class="desc">ご登録用メールアドレスをご準備ください。</p>
                  <p class="p-guide--posting-head-list desc2">
                    携帯電話のメールアドレスをご登録される方は、お使いの携帯電話で、迷惑メール対策の設定や、ドメイン指定受信の設定をされている場合は、解除いただくか、​当ウェブサイトのメールアドレス（＠tokyoartnavi.jp）を受け取れるよう、ドメイン指定受信の追加登録をお願いいたします。
                  </p>
                </div><!-- ./p-guide--update-row-summary -->
                <div class="p-guide--update-row-list">
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>1</span>管理画面にアクセス</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                        <div>
                          <a href="" class="link-icon blank">外部テキストリンク</a>
                        </div>
                    </div>
                  </div>
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>2</span>この文章はダミーコピーです</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </div>
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>3</span>公開依頼</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                        <div>
                          <a href="" class="link-icon blank">外部テキストリンク</a>
                        </div>
                    </div>
                  </div>
                </div><!-- ./p-guide--update-row-list -->
              </div>
            </div><!-- ./p-guide--update-row -->
            <div class="p-guide--update-row" id="update_procedure">
              <h4 class="section-title-ep">更新手順 (情報登録から公開まで)</h4>
              <div class="p-guide--update-row-inner">
                <div class="p-guide--update-row-list">
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>1</span>管理画面にアクセス</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                        <div>
                          <a href="" class="link-icon blank">外部テキストリンク</a>
                        </div>
                    </div>
                  </div>
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>2</span>この文章はダミーコピーです</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </div>
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>3</span>公開依頼</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                        <div>
                          <a href="" class="link-icon blank">外部テキストリンク</a>
                        </div>
                    </div>
                  </div>
                </div><!-- ./p-guide--update-row-list -->
              </div>
            </div><!-- ./p-guide--update-row -->
            <div class="p-guide--update-row" id="correction">
              <h4 class="section-title-ep">公開情報の修正・削除</h4>
              <div class="p-guide--update-row-inner">
                <div class="p-guide--update-row-list">
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>1</span>管理画面にアクセス</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                        <div>
                          <a href="" class="link-icon blank">外部テキストリンク</a>
                        </div>
                    </div>
                  </div>
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>2</span>この文章はダミーコピーです</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                    </div>
                  </div>
                  <div class="p-guide--update-row-item">
                    <div class="p-guide--update-row-item-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                    </div>
                    <div class="p-guide--update-row-item-cnt">
                      <h5 class="p-guide--update-row-item-ttl title-lv4">
                        <span>3</span>公開依頼</h5>
                        <p class="desc2">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
                        <div>
                          <a href="" class="link-icon blank">外部テキストリンク</a>
                        </div>
                    </div>
                  </div>
                </div><!-- ./p-guide--update-row-list -->
              </div>
            </div><!-- ./p-guide--update-row -->
          </div><!-- ./p-guide--update-cnt -->
        </div><!-- ./p-guide--update -->
      </div>
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2">
    <a href="/guide" class="view-more2"><span>ご利用方法トップへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>ご利用方法</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>