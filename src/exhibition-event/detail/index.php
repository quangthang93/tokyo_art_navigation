<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <div class="p-exhibiton--detail">

    <div class="p-exhibiton--detail-head align-center">
      <h1 class="p-end--ttl2">アキバタマビ 21第88回展覧会「いまだかつてあるゆらぎ」</h1>
      <p class="date2 mgb-10">2021/04/03 － 2022/04/03</p>
      <div class="p-exhibiton--detail-head-tags">
        <span class="label-column">NEW</span>
        <span class="label-column green">開催中</span>
        <span class="label-column pink">オンライン</span>
      </div>
    </div><!-- ./p-exhibiton--detail-head -->


    <div class="p-exhibiton--detail-inner">

      <div class="p-exhibiton--detail-sliderWrap">
        <ul class="p-exhibiton--detail-slider js-slider-detail">
          <li>
            <a href="">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/end/detail01.png" alt="">
            </a>
          </li>
          <li>
            <a href="">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/search03.png" alt="">
            </a>
          </li>
          <li>
            <a href="">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/search04.png" alt="">
            </a>
          </li>
        </ul>
      </div>

      <div class="p-exhibiton--detail-cnt">
        <div class="p-exhibiton--detail-infor">
          <p class="p-exhibiton--detail-infor-desc desc3">1950年代、敗戦前の若手エリート官僚が久しぶりに集い久闊(きゅうかつ)を叙(じょ)す。やがて酒が進むうちに話は二人の故人に収斂する。一人は首相・近衛文麿。近衛の最大の失策、日中戦争長期化の経緯が語られる。もう一人は外相・松岡洋右。アメリカの警戒レベルを引き上げた三国同盟締結の経緯が語られる。さらに語られる対米戦への「帰還不能点」南部仏印進駐。大日本帝国を破滅させた文官たちの物語。</p>

          <div class="p-exhibiton--detail-infor-label">
            <span class="tag">美術</span>
            <div class="p-exhibiton--detail-infor-locationWrap">
              <span class="desc2 txt-bold">ガーディアン・ガーデン</span>
              <div>
                <span class="vertical-line"></span>
                <span class="label-map2">東京都（23区内）</span>
              </div>
            </div>
          </div>

          <div class="p-exhibiton--detail-infor-cnt">
            <div class="p-exhibiton--detail-infor-inner">
              <p class="ttl-bold mgb-15">演出</p>
              <p class="desc mgb-15">日澤雄介</p>
              <p class="ttl-bold mgb-15">出演</p>
              <p class="desc mgb-15">浅井伸治 岡本篤 西尾友樹（以上、劇団チョコレートケーキ） 青木柳葉魚（タテヨコ企画） 東谷英人（DULL-COLORED POP） 粟野史浩（文学座） 今里真（ファザーズコーポレーション） 緒方晋（The Stone Age） 村上誠基 黒沢あすか</p>
              <div class="mgb-5">
                <a href="" class="link-icon blank">劇団チョコレートケーキ OFFICIAL SITE</a>
              </div>
              <div class="mgb-5">
                <a href="" class="link-icon">劇団チョコレートケーキ OFFICIAL SITE</a>
              </div>
            </div>
            <div class="p-exhibiton--detail-infor-thumbWrap">
              <div class="p-exhibiton--detail-infor-thumb">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/end/detail02.png" alt="">
              </div>
            </div>
          </div>
          <div class="p-exhibiton--detail-infor-row">
            <h2 class="section-title-ep">インフォメーション</h2>
            <div class="p-exhibiton--detail-infor-rowInner">
              <p class="desc2 mgb-20">各展覧会・イベントの詳細や、休館日・休演日等は、公式ウェブサイトをご確認ください。 新型コロナウイルス感染拡大防止のため、各イベントは中止・変更になる場合があります。最新情報は公式ウェブサイトでご確認ください。</p>
              <p class="desc txt-bold mgb-30">アキバタマビ 21第88回展覧会「いまだかつてあるゆらぎ」</p>
              <div class="table">
                <table>
                  <tr>
                    <th>会期</th>
                    <td>2021/05/02 (日) − 2021/05/09 (日)</td>
                  </tr>
                  <tr>
                    <th>会場</th>
                    <td><a href="" class="link-icon blank type2" target="_blank">東京芸術劇場</a></td>
                  </tr>
                  <tr>
                    <th>開館時間 ・休館日</th>
                    <td>平日：12:00-18:30／土・祝：11:00-18:30</td>
                  </tr>
                  <tr>
                    <th>チケット料金</th>
                    <td>一般1,200円、学生950円、中高生・65歳以上600円</td>
                  </tr>
                  <tr>
                    <th>URL</th>
                    <td><a href="https://tokyoartnavi.jp/talkplay/index004.php" class="link-icon blank type2" target="_blank">https://tokyoartnavi.jp/talkplay/index004.php</a></td>
                  </tr>
                  <tr>
                    <th>お問い合わせ先</th>
                    <td>art gallery closet 03-5469-0355</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>

        </div><!-- ./p-exhibiton--detail-infor -->
        <div class="p-exhibiton--detail-sidebar">
          <p class="ttl-bold en mgb-10">SEARCH</p>
          <form class="form-search" action="">
            <div class="form-search--fields">
              <div class="form-search--fields-inner">
                <div class="form-search--fields-col type2">
                  <div class="form-search--fields-itemWrap type2">
                    <div class="form-search--fields-item">
                      <select class="input select" id="select01">
                        <option>ジャンル</option>
                        <option>キーワード1</option>
                        <option>キーワード2</option>
                      </select>
                    </div>
                    <div class="form-search--fields-item">
                      <select class="input select" id="select02">
                        <option>エリア</option>
                        <option>キーワード1</option>
                        <option>キーワード2</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-search--fields-itemWrap">
                    <div class="form-search--fields-item type2 calendar">
                        <input readonly="readonly" class="input js-datepicker01" type="text" value="日付">
                    </div>
                    <span class="form-search--fields-item-symbol">〜</span>
                    <div class="form-search--fields-item type2 calendar">
                        <input readonly="" class="input js-datepicker01" type="text" value="日付">
                    </div>
                  </div>
                </div>
                <div class="form-search--fields-col">
                  <ul class="list-checkbox">
                    <li>
                      <input class="checkbox type2" id="cbox01" type="checkbox" value="value1">
                      <label for="cbox01">受付中</label>
                    </li>
                    <li>
                      <input class="checkbox" id="cbox02" type="checkbox" value="value1">
                      <label for="cbox02">まもなく受付</label>
                    </li>
                    <li>
                      <input class="checkbox" id="cbox03" type="checkbox" value="value1">
                      <label for="cbox03">まもなく終了</label>
                    </li>
                    <li>
                      <input class="checkbox" id="cbox04" type="checkbox" value="value1">
                      <label for="cbox04">終了</label>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="form-search--submit">
              <a href="" class="btnSM link"><span>検索</span></a>
            </div>
          </form>
        </div><!-- ./p-exhibiton--detail-sidebar -->
      </div>

    </div><!-- ./p-exhibiton--detail-inner -->



  </div><!-- ./p-exhibiton--detail -->


  <div class="align-center fadeup delay-2">
    <a href="/exhibition-event/" class="view-more2"><span>展覧会・イベント情報</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li><a href="/">展覧会・イベント情報</a></li>
        <li>アキバタマビ 21第88回展覧会「いまだかつてあるゆらぎ」</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>