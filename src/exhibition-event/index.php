<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl3">EVENTS</h1>
      <p class="p-end--ttlJP">展覧会・イベント情報</p>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-end--cnt js-supportPoint mgb-80">
    <div class="container5">
      <!-- <div class="p-end--ttlIntroWrap mgb-60">
        <p class="p-end--ttlIntro" href=""><span>現在募集中のコンテスト情報</span></p>
      </div> -->
      <div class="form-searchWrap">
        <a href="" class="form-searchWrap--contact js-support link">
          <span class="form-searchWrap--contact-icon">
            <img class="cover" src="<?php echo $PATH;?>/assets/images/end/ttl-support.svg" alt="">
          </span>
          <span class="form-searchWrap--contact-txt">
            <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-men.png" alt="">
            支援者の<br>みなさまへ
          </span>
        </a>
        <form class="form-search" action="">
          <div class="form-search--fields">
            <div class="form-search--fields-inner">
              <div class="form-search--fields-col type2">
                <div class="form-search--fields-itemWrap">
                  <div class="form-search--fields-item">
                    <select class="input select" id="select01">
                      <option>ジャンル</option>
                      <option>キーワード1</option>
                      <option>キーワード2</option>
                    </select>
                  </div>
                  <div class="form-search--fields-item">
                    <select class="input select" id="select02">
                      <option>エリア</option>
                      <option>キーワード1</option>
                      <option>キーワード2</option>
                    </select>
                  </div>
                </div>
                <div class="form-search--fields-itemWrap">
                  <div class="form-search--fields-item calendar">
                      <input readonly="readonly" class="input js-datepicker01" type="text" value="日付を選択">
                  </div>
                  <span class="form-search--fields-item-symbol">〜</span>
                  <div class="form-search--fields-item calendar">
                      <input readonly="" class="input js-datepicker01" type="text" value="日付を選択">
                  </div>
                </div>
              </div>
              <div class="form-search--fields-col">
                <ul class="list-checkbox">
                  <li>
                    <input class="checkbox type2" id="cbox01" type="checkbox" value="value1">
                    <label for="cbox01">開催中</label>
                  </li>
                  <li>
                    <input class="checkbox" id="cbox02" type="checkbox" value="value1">
                    <label for="cbox02">まもなく開催</label>
                  </li>
                  <li>
                    <input class="checkbox" id="cbox03" type="checkbox" value="value1">
                    <label for="cbox03">まもなく終了</label>
                  </li>
                  <li>
                    <input class="checkbox" id="cbox04" type="checkbox" value="value1">
                    <label for="cbox04">終了</label>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="form-search--submit">
            <a href="" class="btnSM link"><span>検索</span></a>
          </div>
        </form>
      </div>
      <div class="p-artist--results">
        <div class="p-artist--results-labelWrap">
          <p class="p-artist--results-label"><span>100</span>件の情報が登録されています</p>
          <ul class="p-artist--results-cat">
            <li class="active"><a href="">新着順</a></li>
            <li><a href="">終了日が近い順</a></li>
            <li><a href="">人気順</a></li>
          </ul>
        </div>
        <div class="p-artist--results-desc">
          <p class="desc">各展覧会・イベントの詳細や、休館日・休演日等は、公式ウェブサイトをご確認ください。</p>
          <p class="ttl-bold">新型コロナウイルス感染拡大防止のため、各イベントは中止・変更になる場合があります。最新情報は公式ウェブサイトでご確認ください。</p>
        </div>
        <ul class="section-search--list fadeup delay-2">
          <li class="section-search--item type2 fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column blue">まもなく開催</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search01.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column blue">まもなく開催</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">特別展「国立ベルリン・エジプト博物館所蔵　古代エジプト展　天地創造の神話」<span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">The Artcomplex Center of Tokyo／アートコンプレックスセンター</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item type2 fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column blue">まもなく開催</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search02.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column blue">まもなく開催</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">佐々木琢磨 15周年記念絵画展ツアー2021,2022 ALL WE ARE <span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">並樹画廊</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item type2 fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column green">開催中</span>
                <span class="label-column purple">オンライン</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search03.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column green">開催中</span>
                    <span class="label-column purple">オンライン</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">NAKED QUILT販売会<span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">Room_412</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item type2 fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column blue">まもなく開催</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search04.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column blue">まもなく開催</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">特別展「国立ベルリン・エジプト博物館所蔵　古代エジプト展　天地創造の神話」<span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">The Artcomplex Center of Tokyo／アートコンプレックスセンター</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item type2 fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column blue">まもなく開催</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search02.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column blue">まもなく開催</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">佐々木琢磨 15周年記念絵画展ツアー2021,2022 ALL WE ARE <span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">並樹画廊</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item type2 fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column green">開催中</span>
                <span class="label-column purple">オンライン</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search03.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column green">開催中</span>
                    <span class="label-column purple">オンライン</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">NAKED QUILT販売会<span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">Room_412</p>
                </div>
              </div>
            </a>
          </li>
        </ul>
        <div class="pagination">
          <p class="pagination-label">50件中｜1〜20件 表示</p>
          <div class="pagination-list">
            <a class="ctrl prev" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                <path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(-717 4092) rotate(90)" /></svg>
            </a>
            <a class="active" href="">1</a>
            <a href="">2</a>
            <a href="">3</a>
            <a href="">4</a>
            <a href="">5</a>
            <a href="">6</a>
            <div class="pagination-spacer">…</div>
            <a href="">12</a>
            <a class="ctrl next" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
                <path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(723 -4082) rotate(-90)" /></svg>
            </a>
          </div>
        </div>
        <div class="p-artist--directs">
          <div class="p-artist--directs-cnt">
            <p class="ttl-bold">アーティスト支援者の方へ</p>
            <p class="desc2">Tokyo Art Navigationでは、アーティストの創作活動を支援する助成金や文化活動への支援情報を随時募集しています。</p>
          </div>
          <div class="p-artist--directs-link">
            <a class="view-more" href="">詳しくはこちら</a>
          </div>
        </div>
        <div class="align-center fadeup delay-2">
          <a href="" class="view-more2"><span>トップページへ</span></a>
        </div>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>アーティスト支援制度</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>