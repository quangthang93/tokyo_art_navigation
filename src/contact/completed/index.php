<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl2 type2">
        お問い合わせ
      </h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-news--detail-inner">
      <div class="v-contact">
        <div class="u-layout-smaller">
          <div class="c-steps">
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">1</span>
              <p></p>
              <p class="c-steps__col__label">問い合わせ内容入力</p>
              <p></p>
            </div>
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">2</span>
              <p></p>
              <p class="c-steps__col__label">内容確認</p>
              <p></p>
            </div>
            <div class="c-steps__col is-active">
              <span class="c-steps__col__number u-font-rajdhani">3</span>
              <p></p>
              <p class="c-steps__col__label">完了</p>
              <p></p>
            </div>
          </div>
          <p class="c-contact__message type2">送信完了しました。</p>
          <p class="c-contact__message type3">お問い合わせ送信が完了いたしました。担当スタッフが内容を確認し、ご連絡させていただきます。 <br>しばらく経っても連絡がない場合は、恐れ入りますが、再度お問い合わせください。</p>
        </div>
      </div><!-- ./v-contact -->
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2 mgt-50">
    <a href="/guide" class="view-more2"><span>トップページへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>お問い合わせ</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>