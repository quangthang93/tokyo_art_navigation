<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl2 type2">
        お問い合わせ
      </h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-news--detail-inner">
      <div class="v-contact">
      <div class="u-layout-smaller">
        <div class="c-steps">
          <div class="c-steps__col is-active">
            <span class="c-steps__col__number u-font-rajdhani">1</span>
            <p></p>
            <p class="c-steps__col__label">問い合わせ内容入力</p>
            <p></p>
          </div>
          <div class="c-steps__col">
            <span class="c-steps__col__number u-font-rajdhani">2</span>
            <p></p>
            <p class="c-steps__col__label">内容確認</p>
            <p></p>
          </div>
          <div class="c-steps__col">
            <span class="c-steps__col__number u-font-rajdhani">3</span>
            <p></p>
            <p class="c-steps__col__label">完了</p>
            <p></p>
          </div>
        </div>
        <p class="c-contact__message">お問い合わせご希望の方は下記フォームよりお送り下さい。<br>送信内容を確認させていただき担当よりご連絡致します。</p>
        <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
          <form method="post" action="" enctype="multipart/form-data">
            <div class="c-form">
              <div class="c-form__row"><label for="" class="c-form__row__label"><span class="c-form__row__label__text">お問い合わせ種別</span> <span class="c-form__required">必須</span> </label>
                <div class="c-form__row__field">
                  <div class="c-form__radio"> <span class="mwform-radio-field horizontal-item">
                      <label>
                        <input type="radio" name="type" value="このサイトについて" class="horizontal-item">
                        <span class="mwform-radio-field-text">このサイトについて</span>
                      </label>
                    </span>
                    <span class="mwform-radio-field horizontal-item">
                      <label>
                        <input type="radio" name="type" value="掲載情報について" class="horizontal-item">
                        <span class="mwform-radio-field-text">掲載情報について</span>
                      </label>
                    </span>
                    <input type="hidden" name="__children[type][]" value="{&quot;\u88fd\u54c1\u306e\u3054\u76f8\u8ac7&quot;:&quot;\u88fd\u54c1\u306e\u3054\u76f8\u8ac7&quot;,&quot;\u8cc7\u6599\u8acb\u6c42&quot;:&quot;\u8cc7\u6599\u8acb\u6c42&quot;}">
                  </div>
                </div>
              </div>
              <div class="c-form__row is-vertical-top"><label for="content" class="c-form__row__label"><span class="c-form__row__label__text">お問い合わせ内容</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field"><textarea name="content" id="content" class="c-form__textarea" cols="50" rows="7" placeholder="入力してください"></textarea>
                </div>
              </div>
              <div class="c-form__row"><label for="email" class="c-form__row__label"><span class="c-form__row__label__text">メールアドレス</span> <span class="c-form__required">必須</span> </label>
                <p></p>
                <div class="c-form__row__field"><input type="email" name="email" id="email" class="c-form__input" size="60" value="" placeholder="例) example@xxxxxx.co.jp" data-conv-half-alphanumeric="true">
                </div>
              </div>
              <div class="c-form__row"><label for="name" class="c-form__row__label"><span class="c-form__row__label__text">お名前(全角)</span></label>
                <p></p>
                <div class="c-form__row__field"><input type="text" name="name" id="name" class="c-form__input" size="60" value="" placeholder="例）山田太郎">
                </div>
              </div>
              <div class="c-form__row"><label for="phonetic" class="c-form__row__label"><span class="c-form__row__label__text">ふりがな(全角)</span></label>
                <p></p>
                <div class="c-form__row__field"><input type="text" name="phonetic" id="phonetic" class="c-form__input" size="60" value="" placeholder="例）やまだたろう">
                </div>
              </div>
              <div class="c-form__row"><label for="pref" class="c-form__row__label"><span class="c-form__row__label__text">お住まいのエリア</span></label>
                <div class="c-form__row__field">
                  <div class="d-flex mgb-10">
                    <div class="d-flex--ttl">都道府県</div>
                    <div class="c-form__select"><select name="pref" class="c-form__select__field">
                        <option value="" selected="selected">
                          選択してください </option>
                        <option value="北海道">
                          北海道 </option>
                        <option value="青森県">
                          青森県 </option>
                        <option value="岩手県">
                          岩手県 </option>
                        <option value="宮城県">
                          宮城県 </option>
                        <option value="秋田県">
                          秋田県 </option>
                        <option value="山形県">
                          山形県 </option>
                        <option value="福島県">
                          福島県 </option>
                        <option value="茨城県">
                          茨城県 </option>
                        <option value="栃木県">
                          栃木県 </option>
                        <option value="群馬県">
                          群馬県 </option>
                        <option value="埼玉県">
                          埼玉県 </option>
                        <option value="千葉県">
                          千葉県 </option>
                        <option value="東京都">
                          東京都 </option>
                        <option value="神奈川県">
                          神奈川県 </option>
                        <option value="山梨県">
                          山梨県 </option>
                        <option value="長野県">
                          長野県 </option>
                        <option value="新潟県">
                          新潟県 </option>
                        <option value="富山県">
                          富山県 </option>
                        <option value="石川県">
                          石川県 </option>
                        <option value="福井県">
                          福井県 </option>
                        <option value="岐阜県">
                          岐阜県 </option>
                        <option value="静岡県">
                          静岡県 </option>
                        <option value="愛知県">
                          愛知県 </option>
                        <option value="三重県">
                          三重県 </option>
                        <option value="滋賀県">
                          滋賀県 </option>
                        <option value="京都府">
                          京都府 </option>
                        <option value="大阪府">
                          大阪府 </option>
                        <option value="兵庫県">
                          兵庫県 </option>
                        <option value="奈良県">
                          奈良県 </option>
                        <option value="和歌山県">
                          和歌山県 </option>
                        <option value="鳥取県">
                          鳥取県 </option>
                        <option value="島根県">
                          島根県 </option>
                        <option value="岡山県">
                          岡山県 </option>
                        <option value="広島県">
                          広島県 </option>
                        <option value="山口県">
                          山口県 </option>
                        <option value="徳島県">
                          徳島県 </option>
                        <option value="香川県">
                          香川県 </option>
                        <option value="愛媛県">
                          愛媛県 </option>
                        <option value="高知県">
                          高知県 </option>
                        <option value="福岡県">
                          福岡県 </option>
                        <option value="佐賀県">
                          佐賀県 </option>
                        <option value="長崎県">
                          長崎県 </option>
                        <option value="熊本県">
                          熊本県 </option>
                        <option value="大分県">
                          大分県 </option>
                        <option value="宮崎県">
                          宮崎県 </option>
                        <option value="鹿児島県">
                          鹿児島県 </option>
                        <option value="沖縄県">
                          沖縄県 </option>
                      </select>
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="c-form__select__icon">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"></path>
                      </svg></div>
                  </div>
                  <div class="d-flex">
                    <div class="d-flex--ttl">市区町村</div>
                    <input type="text" name="phonetic" id="phonetic" class="c-form__input" size="60" value="" placeholder="入力してください">
                  </div>
                </div>
              </div>
              <div class="c-form__row"><label for="address1" class="c-form__row__label"><span class="c-form__row__label__text">訪れたことのある<br>美術館・博物館・ホール</span></label>
                <div class="c-form__row__field">
                  <div class="d-flex2">
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb01">
                            <input type="checkbox" name="privacy" value="privacy" id="cb01" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京都庭園美術館</span>
                          </label>
                        </span>
                      </div>
                    </div>
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb02">
                            <input type="checkbox" name="privacy" value="privacy" id="cb02" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京都江戸東京博物館</span>
                          </label>
                        </span>
                      </div>
                    </div>   
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb03">
                            <input type="checkbox" name="privacy" value="privacy" id="cb03" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">江戸東京たてもの園</span>
                          </label>
                        </span>
                      </div>
                    </div>  
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb04">
                            <input type="checkbox" name="privacy" value="privacy" id="cb04" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京都写真美術館</span>
                          </label>
                        </span>
                      </div>
                    </div>  
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb05">
                            <input type="checkbox" name="privacy" value="privacy" id="cb05" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京都現代美術館</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb06">
                            <input type="checkbox" name="privacy" value="privacy" id="cb06" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京都渋谷公園通りギャラリー</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb07">
                            <input type="checkbox" name="privacy" value="privacy" id="cb07" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">トーキョーアーツアンドスペース</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb08">
                            <input type="checkbox" name="privacy" value="privacy" id="cb08" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京都美術館</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb09">
                            <input type="checkbox" name="privacy" value="privacy" id="cb09" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京文化会館</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb10">
                            <input type="checkbox" name="privacy" value="privacy" id="cb10" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京芸術劇場</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb11">
                            <input type="checkbox" name="privacy" value="privacy" id="cb11" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">東京舞台芸術活動支援センター(水天宮ピット)</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb12">
                            <input type="checkbox" name="privacy" value="privacy" id="cb12" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">アーツカウンシル東京</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                    <div class="d-flex2--item">
                      <div class="c-form__checkbox">
                        <span class="mwform-checkbox-field horizontal-item">
                          <label for="cb13">
                            <input type="checkbox" name="privacy" value="privacy" id="cb13" class="c-form__checkbox__field">
                            <span class="mwform-checkbox-field-text">いずれも訪れたことがない</span>
                          </label>
                        </span>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>             
              
            </div>
            <div class="c-contact__privacy is-hide-confirm">
              <div class="c-contact__privacy__inner">
                <p class="c-contact__privacy__text">当社は、お客様個人を識別できる情報（以下「個人情報」といいます。）を適切に保護する為に、以下の取り組みを実施いたします。</p>
                <dl class="c-contact__privacy__list">
                  <dt class="c-contact__privacy__list__title">1.法令の遵守</dt>
                  <dd class="c-contact__privacy__list__data">当社は、個人情報保護に関する関係法令、国が定める指針等及び社内規程を遵守致します。</dd>
                  <dt class="c-contact__privacy__list__title">2.個人情報の取得</dt>
                  <dd class="c-contact__privacy__list__data">当社は、個人情報を取得する際には、その利用目的を明示し、お客様の同意の範囲内で、適正かつ公正な手段によって取得いたします。</dd>
                  <dt class="c-contact__privacy__list__title">3.利用目的</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様からご提供いただいた個人情報を、お客様とのご契約上の責務を果たすため、およびお客様に有用な情報をご提供するために利用いたします。</dd>
                  <dt class="c-contact__privacy__list__title">4.第三者提供</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様の個人情報をあらかじめお客様の同意をいただいている場合および法令等で定められた場合、または当社と機密保持契約を締結している業務委託先に利用目的の達成に必要な範囲内で開示する場合を除き、第三者へ開示いたしません。</dd>
                  <dt class="c-contact__privacy__list__title">5.管理体制</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様の個人情報保護のため、情報管理責任者や個人情報を取り扱う部門毎に部門別情報管理者を置き、個人情報の適切な管理に努めます。 また、業務に従事する者に対して適切な教育を実施いたします。</dd>
                  <dt class="c-contact__privacy__list__title">6.安全管理措置</dt>
                  <dd class="c-contact__privacy__list__data">当社は、お客様からご提供いただいた個人情報に対して、不正アクセス・紛失・漏洩などを防止するためのセキュリティ対策を実施いたします。</dd>
                </dl>
              </div>
            </div>
            <div class="c-contact__privacy-check is-hide-confirm">
              <div class="c-form__checkbox">
                <span class="mwform-checkbox-field horizontal-item">
                  <label for="privacy-1">
                    <input type="checkbox" name="privacy" value="privacy" id="privacy-1" class="c-form__checkbox__field">
                    <span class="mwform-checkbox-field-text">個人情報保護方針に同意する</span>
                  </label>
                </span>
              </div>
              <p></p>
            </div>
            <ul class="c-contact__action">
              <li><input type="submit" name="submitConfirm" value="個人情報保護方針に同意する" class="c-contact__action__button c-button is-yellow">
              </li>
            </ul>
          </form>
          <!-- end .mw_wp_form -->
        </div>
      </div>
    </div><!-- ./v-contact -->
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2 mgt-50">
    <a href="/guide" class="view-more2"><span>トップページへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>お問い合わせ</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>