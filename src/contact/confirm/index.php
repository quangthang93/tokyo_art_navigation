<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl2 type2">
        お問い合わせ
      </h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-news--detail">
    <div class="p-news--detail-inner">
      <div class="v-contact">
        <div class="u-layout-smaller">
          <div class="c-steps">
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">1</span>
              <p></p>
              <p class="c-steps__col__label">問い合わせ内容入力</p>
              <p></p>
            </div>
            <div class="c-steps__col is-active">
              <span class="c-steps__col__number u-font-rajdhani">2</span>
              <p></p>
              <p class="c-steps__col__label">内容確認</p>
              <p></p>
            </div>
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">3</span>
              <p></p>
              <p class="c-steps__col__label">完了</p>
              <p></p>
            </div>
          </div>
          <p class="c-contact__message">下記の内容でよろしければ、「送信」ボタンを<br class="pc-only">クリックしてください。</p>
          <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
            <form method="post" action="" enctype="multipart/form-data">
              <div class="c-form">
                <div class="c-form__row">
                  <label for="nickname" class="c-form__row__label">
                    <span class="c-form__row__label__text">お問い合わせ種別</span>
                  </label>
                  <div class="c-form__row__field type2">
                    このサイトについて <input type="hidden" name="fullname" value="このサイトについて">
                  </div>
                </div>
                <div class="c-form__row">
                  <label for="nickname" class="c-form__row__label">
                    <span class="c-form__row__label__text">お問い合わせ内容</span>
                  </label>
                  <div class="c-form__row__field type2">
                    お問い合わせ内容が入ります。 <input type="hidden" name="fullname" value="お問い合わせ内容が入ります。">
                  </div>
                </div>
                <div class="c-form__row">
                  <label for="nickname" class="c-form__row__label">
                    <span class="c-form__row__label__text">メールアドレス</span>
                  </label>
                  <div class="c-form__row__field type2">
                    example@xxxx.co.jp <input type="hidden" name="fullname" value="example@xxxx.co.jp">
                  </div>
                </div>
                <div class="c-form__row">
                  <label for="nickname" class="c-form__row__label">
                    <span class="c-form__row__label__text">お名前(全角)</span>
                  </label>
                  <div class="c-form__row__field type2">
                    山田太郎 <input type="hidden" name="fullname" value="山田太郎">
                  </div>
                </div>
                <div class="c-form__row">
                  <label for="nickname" class="c-form__row__label">
                    <span class="c-form__row__label__text">お住まいのエリア</span>
                  </label>
                  <div class="c-form__row__field type2">
                    <div class="mgb-15">
                      東京都 <input type="hidden" name="fullname" value="東京都">
                    </div>
                    <div>
                      墨田区 <input type="hidden" name="fullname" value="墨田区">
                    </div>
                  </div>
                </div>
                <div class="c-form__row">
                  <label for="nickname" class="c-form__row__label">
                    <span class="c-form__row__label__text">訪れたことのある<br>美術館・博物館・ホール</span>
                  </label>
                  <div class="c-form__row__field type2">
                      東京都庭園美術館 , 東京都江戸東京博物館 <input type="hidden" name="fullname" value="東京都庭園美術館 , 東京都江戸東京博物館">
                  </div>
                </div>
              </div>
              <ul class="c-contact__action">
                <li><input type="submit" name="submitConfirm" value="修正する" class="c-contact__action__button c-button is-gray">
                </li>
                <li><input type="submit" name="submitConfirm" value="送信する" class="c-contact__action__button c-button is-yellow">
              </ul>
            </form>
            <!-- end .mw_wp_form -->
          </div>
        </div>
      </div><!-- ./v-contact -->
    </div><!-- ./p-news--detail-inner -->
  </div><!-- ./p-news--detail -->
  <div class="align-center fadeup delay-2 mgt-50">
    <a href="/guide" class="view-more2"><span>トップページへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>お問い合わせ</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>