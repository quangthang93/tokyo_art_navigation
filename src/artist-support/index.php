<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl3">SUPPORTS</h1>
      <p class="p-end--ttlJP">支援情報</p>
    </div>
  </section><!-- ./p-recruit--banner -->

  <div class="p-end--cnt js-supportPoint mgb-80">
    <div class="container5">
      <!-- <div class="p-end--ttlIntroWrap mgb-60">
        <p class="p-end--ttlIntro" href=""><span>現在募集中のアーティスト支援制度</span></p>
      </div> -->
      <div class="form-searchWrap">
        <a href="" class="form-searchWrap--contact link js-support">
          <span class="form-searchWrap--contact-icon">
            <img class="cover" src="<?php echo $PATH;?>/assets/images/end/ttl-support.svg" alt="">
          </span>
          <span class="form-searchWrap--contact-txt">
            <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-men.png" alt="">
            支援者の<br>みなさまへ
          </span>
        </a>
        <form class="form-search" action="">
          <div class="form-search--fields">
            <div class="form-search--fields-col left">
              <select class="select input" id="select01">
                <option>ジャンルを絞り込む</option>
                <option>キーワード1</option>
                <option>キーワード2</option>
              </select>
            </div>
            <div class="form-search--fields-col">
              <ul class="list-checkbox">
                <li>
                  <input class="checkbox type2" id="cbox01" type="checkbox" value="value1">
                  <label for="cbox01">通年開催</label>
                </li>
                <li>
                  <input class="checkbox" id="cbox02" type="checkbox" value="value1">
                  <label for="cbox02">受付前</label>
                </li>
                <li>
                  <input class="checkbox" id="cbox03" type="checkbox" value="value1">
                  <label for="cbox03">受付中</label>
                </li>
                <li>
                  <input class="checkbox" id="cbox04" type="checkbox" value="value1">
                  <label for="cbox04">まもなく終了</label>
                </li>
                <li>
                  <input class="checkbox" id="cbox05" type="checkbox" value="value1">
                  <label for="cbox05">終了</label>
                </li>
              </ul>
            </div>
          </div>
          <div class="form-search--submit">
            <a href="" class="btnSM link"><span>検索</span></a>
          </div>
        </form>
      </div>
      <div class="p-artist--results">
        <div class="p-artist--results-labelWrap">
          <p class="p-artist--results-label"><span>50</span>件の情報が登録されています</p>
        </div>
        <ul class="section-support--list fadeup delay-2">
          <li class="section-support--item">
            <a href="" class="link ttl-ani-underlineWrap">
              <div class="section-support--item-inner">
                <div class="section-support--item-label">
                  <span class="label-column">NEW</span>
                  <span class="label-column pink">通年開催</span>
                  <span class="label-column green">受付中</span>
                </div>
                <h3 class="ttl-bold ttl-ani-underline">芸術文化活動支援事業「アートにエールを！東京プロジェクト（ステージ型）」</h3>
                <p class="desc2">東京都、公益財団法人東京都歴史文化財団</p>
                <div class="section-support--item-tags">
                  <span class="tag">美術</span>
                  <span class="tag">音楽</span>
                  <span class="tag">パフォーマンス</span>
                  <span class="tag">その他</span>
                </div>
                <p class="date">2021.04.03 - 2022.04.03</p>
              </div>
            </a>
          </li>
          <li class="section-support--item">
            <a href="" class="link ttl-ani-underlineWrap">
              <div class="section-support--item-inner">
                <div class="section-support--item-label">
                  <span class="label-column">NEW</span>
                  <span class="label-column blue">受付前</span>
                </div>
                <h3 class="ttl-bold ttl-ani-underline">芸術文化活動支援事業「アートにエールを！東京プロジェクト（ステージ型）」</h3>
                <p class="desc2">東京都、公益財団法人東京都歴史文化財団</p>
                <div class="section-support--item-tags">
                  <span class="tag">美術</span>
                  <span class="tag">音楽</span>
                  <span class="tag">パフォーマンス</span>
                  <span class="tag">その他</span>
                </div>
                <p class="date">2021.04.03 - 2022.04.03</p>
              </div>
            </a>
          </li>
          <li class="section-support--item">
            <a href="" class="link ttl-ani-underlineWrap">
              <div class="section-support--item-inner">
                <div class="section-support--item-label">
                  <span class="label-column">NEW</span>
                  <span class="label-column gray">まもなく終了</span>
                </div>
                <h3 class="ttl-bold ttl-ani-underline">芸術文化活動支援事業「アートにエールを！東京プロジェクト（ステージ型）」</h3>
                <p class="desc2">東京都、公益財団法人東京都歴史文化財団</p>
                <div class="section-support--item-tags">
                  <span class="tag">美術</span>
                  <span class="tag">音楽</span>
                  <span class="tag">パフォーマンス</span>
                  <span class="tag">その他</span>
                </div>
                <p class="date">2021.04.03 - 2022.04.03</p>
              </div>
            </a>
          </li>
          <li class="section-support--item completed">
            <a href="" class="link ttl-ani-underlineWrap">
              <div class="section-support--item-inner">
                <div class="section-support--item-label">
                  <span class="label-column gray2">終了</span>
                </div>
                <h3 class="ttl-bold ttl-ani-underline">芸術文化活動支援事業「アートにエールを！東京プロジェクト（ステージ型）」</h3>
                <p class="desc2">東京都、公益財団法人東京都歴史文化財団</p>
                <div class="section-support--item-tags">
                  <span class="tag">美術</span>
                  <span class="tag">音楽</span>
                  <span class="tag">パフォーマンス</span>
                  <span class="tag">その他</span>
                </div>
                <p class="date">2021.04.03 - 2022.04.03</p>
              </div>
            </a>
          </li>
          <li class="section-support--item">
            <a href="" class="link ttl-ani-underlineWrap">
              <div class="section-support--item-inner">
                <div class="section-support--item-label">
                  <span class="label-column">NEW</span>
                  <span class="label-column pink">通年開催</span>
                  <span class="label-column green">受付中</span>
                </div>
                <h3 class="ttl-bold ttl-ani-underline">芸術文化活動支援事業「アートにエールを！東京プロジェクト（ステージ型）」</h3>
                <p class="desc2">東京都、公益財団法人東京都歴史文化財団</p>
                <div class="section-support--item-tags">
                  <span class="tag">美術</span>
                  <span class="tag">音楽</span>
                  <span class="tag">パフォーマンス</span>
                  <span class="tag">その他</span>
                </div>
                <p class="date">2021.04.03 - 2022.04.03</p>
              </div>
            </a>
          </li>
        </ul>

        <div class="pagination">
          <p class="pagination-label">50件中｜1〜20件 表示</p>
          <div class="pagination-list">
            <a class="ctrl prev" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"><path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(-717 4092) rotate(90)"/></svg>
            </a>
            <a class="active" href="">1</a>
            <a href="">2</a>
            <a href="">3</a>
            <a href="">4</a>
            <a href="">5</a>
            <a href="">6</a>
            <div class="pagination-spacer">…</div>
            <a href="">12</a>
            <a class="ctrl next" href="">
              <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"><path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(723 -4082) rotate(-90)"/></svg>
            </a>
          </div>
        </div>

        <div class="p-artist--directs">
          <div class="p-artist--directs-cnt">
            <p class="ttl-bold">アーティスト支援者の方へ</p>
            <p class="desc2">Tokyo Art Navigationでは、アーティストの創作活動を支援する助成金や文化活動への支援情報を随時募集しています。</p>
          </div> 
          <div class="p-artist--directs-link">
            <a class="view-more" href="">詳しくはこちら</a>
          </div>
        </div>

        <div class="align-center fadeup delay-2">
          <a href="" class="view-more2"><span>トップページへ</span></a>
        </div>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->


  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->


</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>アーティスト支援制度</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>