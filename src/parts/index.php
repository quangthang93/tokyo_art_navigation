<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <div class="p-news--detail">

    <div class="p-news--detail-head align-center">
      <h1 class="p-end--ttl2">ページタイトル</h1>
    </div><!-- ./p-news--detail-head -->


    <div class="p-news--detail-inner">
      <div class="p-news--detail-cnt">
        <div class="no-reset">
        	<div class="p-news--detail-infor">
        		<h2>見出しレベル2</h2>
        	
        		<div class="mgb-50">
        			<h3>見出しレベル３</h3>
        			<h4>見出しレベル４</h4>
        	      <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
        	
        	      <h4>見出しレベル４</h4>
        	      <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
        	
        	      <div class="mgb-50">
        	        <div><a href="" class="link-icon type3 blank">外部テキストリンク OFFICIAL SITE</a></div>
        	        <div><a href="" class="link-icon type3 pdf2">PDFリンク</a></div>
        	        <div><a href="" class="link-icon type3">外部テキストリンク OFFICIAL SITE</a></div>
        	      </div>

        	      <h4>リスト文字</h4>
        	      <div class="mgb-20">
        	      	<ul>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      	</ul>
        	      </div>
        	      <div class="mgb-20">
        	      	<ol>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      		<li>この文章はダミーコピーですお読みにならないで下さい。</li>
        	      	</ol>
        	      </div>
        	      <div class="mgt-50 border-top border-bottom">
        	      	<h4>見出しレベル４</h4>
        	      	<p style="padding-bottom: 20px;">この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>
        	      </div>
        		</div>

        		<div>
        			<h3>見出しレベル３</h3>
        			<h4>表組み</h4>
    	        <table>
    	          <tr>
    	            <th>会期</th>
    	            <td>2021/05/02 (日) − 2021/05/09 (日)</td>
    	          </tr>
    	          <tr>
    	            <th>会場</th>
    	            <td><a href="" class="link-icon blank type2" target="_blank">東京芸術劇場</a></td>
    	          </tr>
    	          <tr>
    	            <th>開館時間 ・休館日</th>
    	            <td>平日：12:00-18:30／土・祝：11:00-18:30</td>
    	          </tr>
    	          <tr>
    	            <th>チケット料金</th>
    	            <td>一般1,200円、学生950円、中高生・65歳以上600円</td>
    	          </tr>
    	          <tr>
    	            <th>URL</th>
    	            <td><a href="https://tokyoartnavi.jp/talkplay/index004.php" class="link-icon blank type2" target="_blank">https://tokyoartnavi.jp/talkplay/index004.php</a></td>
    	          </tr>
    	          <tr>
    	            <th>お問い合わせ先</th>
    	            <td>art gallery closet 03-5469-0355</td>
    	          </tr>
    	        </table>
        		</div>

        </div>

        </div><!-- ./p-news--detail-infor -->
      </div>

    </div><!-- ./p-news--detail-inner -->



  </div><!-- ./p-news--detail -->


  <div class="align-center fadeup delay-2">
    <a href="/" class="view-more2"><span>トップページへ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li><a href="/">展覧会・イベント情報</a></li>
        <li>アキバタマビ 21第88回展覧会「いまだかつてあるゆらぎ」</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>