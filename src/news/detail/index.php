<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <div class="p-news--detail">

    <div class="p-news--detail-head align-center">
      <h1 class="p-end--ttl2">東京文化会館 オペラ『Only the Sound Remains ―余韻ー』へのご招待</h1>
      <p class="date">2021.00.00</p>
      <div class="p-news--detail-head-tags">
        <span class="label-column pink2">キャンペーン</span>
      </div>
    </div><!-- ./p-news--detail-head -->


    <div class="p-news--detail-inner">

      <div class="p-news--detail-sliderWrap">
        <ul class="p-news--detail-slider">
          <li>
            <a>
              <img class="cover" src="<?php echo $PATH;?>/assets/images/end/detail03.png" alt="">
            </a>
          </li>
          <!-- <li>
            <a href="">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/search03.png" alt="">
            </a>
          </li>
          <li>
            <a href="">
              <img class="cover" src="<?php echo $PATH;?>/assets/images/top/search04.png" alt="">
            </a>
          </li> -->
        </ul>
      </div>

      <div class="p-news--detail-cnt">
        <div class="p-news--detail-infor">
          <p class="p-news--detail-infor-desc desc3">東京文化会館で6/6(日）に開催のオペラ『Only the Sound Remains ―余韻ー』に、学生・教職員の方 20名様を無料でご招待します。（先着順・要事前申込）</p>

          <div class="p-news--detail-label">
            <span class="p-news--detail-sns-ttl">Share</span>
            <div class="p-news--detail-sns-icons">
              <a href="" class="link fb">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook2.svg" alt="">
              </a>
              <a href="" class="link">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter2.svg" alt="">
              </a>
              <a href="" class="link">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-line.svg" alt="">
              </a>
            </div>
          </div>

          <div class="p-news--detail-infor-row">
            <div class="no-reset">
              <h4>見出しレベル４</h4>
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>

              <h4>見出しレベル４</h4>
              <p>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</p>

              <div class="mgb-50">
                <div><a href="" class="link-icon type3 blank">外部テキストリンク OFFICIAL SITE</a></div>
                <div><a href="" class="link-icon type3 pdf2">PDFリンク</a></div>
                <div><a href="" class="link-icon type3">外部テキストリンク OFFICIAL SITE</a></div>
              </div>

              <h3>見出しレベル３</h3>
              
              <div class="p-news--detail-infor-rowInner">
                <p class="mgb-20">各展覧会・イベントの詳細や、休館日・休演日等は、公式ウェブサイトをご確認ください。 新型コロナウイルス感染拡大防止のため、各イベントは中止・変更になる場合があります。最新情報は公式ウェブサイトでご確認ください。</p>
                
                <h4>見出しレベル４</h4>
                <table>
                  <tr>
                    <th>会期</th>
                    <td>2021/05/02 (日) − 2021/05/09 (日)</td>
                  </tr>
                  <tr>
                    <th>会場</th>
                    <td><a href="" class="link-icon blank type2" target="_blank">東京芸術劇場</a></td>
                  </tr>
                  <tr>
                    <th>開館時間 ・休館日</th>
                    <td>平日：12:00-18:30／土・祝：11:00-18:30</td>
                  </tr>
                  <tr>
                    <th>チケット料金</th>
                    <td>一般1,200円、学生950円、中高生・65歳以上600円</td>
                  </tr>
                  <tr>
                    <th>URL</th>
                    <td><a href="https://tokyoartnavi.jp/talkplay/index004.php" class="link-icon blank type2" target="_blank">https://tokyoartnavi.jp/talkplay/index004.php</a></td>
                  </tr>
                  <tr>
                    <th>お問い合わせ先</th>
                    <td>art gallery closet 03-5469-0355</td>
                  </tr>
                </table>
              </div>
            </div>

          </div>

        </div><!-- ./p-news--detail-infor -->
      </div>

    </div><!-- ./p-news--detail-inner -->



  </div><!-- ./p-news--detail -->


  <div class="align-center fadeup delay-2">
    <a href="/news/" class="view-more2"><span>ニュース一覧へ</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li><a href="/">展覧会・イベント情報</a></li>
        <li>アキバタマビ 21第88回展覧会「いまだかつてあるゆらぎ」</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>