<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl3">NEWS</h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-end--cnt mgb-80">
    <div class="container5">
      <ul class="p-news--list">
        <li class="p-news--item link">
          <a href="/news/detail">
            <div class="p-news--item-dateWrap">
              <span class="date">2021.00.00</span>
              <span class="label-column blue2">お知らせ</span>
            </div>
            <p class="desc4 ttl-ani-underline">システムメンテナンスのお知らせ</p>
          </a>
        </li>
        <li class="p-news--item link">
          <a href="/news/detail">
            <div class="p-news--item-dateWrap">
              <span class="date">2021.00.00</span>
              <span class="label-column blue2">お知らせ</span>
            </div>
            <p class="desc4 ttl-ani-underline"><span class="link-icon2 blank">トーキョーアーツアンドスペース「Tokyo Contemporary Art Award 2022-2024」 6/14より公募開始！</span></p>
          </a>
        </li>
        <li class="p-news--item link">
          <a href="/news/detail">
            <div class="p-news--item-dateWrap">
              <span class="date">2021.00.00</span>
              <span class="label-column pink2">キャンペーン</span>
            </div>
            <p class="desc4 ttl-ani-underline">東京文化会館 オペラ『Only the Sound Remains ―余韻ー』へのご招待</p>
          </a>
        </li>
        <li class="p-news--item link">
          <a href="/news/detail">
            <div class="p-news--item-dateWrap">
              <span class="date">2021.00.00</span>
              <span class="label-column blue2">お知らせ</span>
            </div>
            <p class="desc4 ttl-ani-underline">システムメンテナンスのお知らせ</p>
          </a>
        </li>
        <li class="p-news--item link">
          <a href="/news/detail">
            <div class="p-news--item-dateWrap">
              <span class="date">2021.00.00</span>
              <span class="label-column blue2">お知らせ</span>
            </div>
            <p class="desc4 ttl-ani-underline"><span class="link-icon2 pdf">トーキョーアーツアンドスペース「Tokyo Contemporary Art Award 2022-2024」 6/14より公募開始！</span></p>
          </a>
        </li>
        <li class="p-news--item link">
          <a href="/news/detail">
            <div class="p-news--item-dateWrap">
              <span class="date">2021.00.00</span>
              <span class="label-column pink2">キャンペーン</span>
            </div>
            <p class="desc4 ttl-ani-underline">東京文化会館 オペラ『Only the Sound Remains ―余韻ー』へのご招待</p>
          </a>
        </li>
      </ul>

      <div class="pagination">
        <p class="pagination-label">50件中｜1〜20件 表示</p>
        <div class="pagination-list">
          <a class="ctrl prev" href="">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"><path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(-717 4092) rotate(90)"/></svg>
          </a>
          <a class="active" href="">1</a>
          <a href="">2</a>
          <a href="">3</a>
          <a href="">4</a>
          <a href="">5</a>
          <a href="">6</a>
          <div class="pagination-spacer">…</div>
          <a href="">12</a>
          <a class="ctrl next" href="">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"><path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(723 -4082) rotate(-90)"/></svg>
          </a>
        </div>
      </div>
      <div class="align-center mgt-60 fadeup delay-2">
        <a href="" class="view-more2"><span>トップページへ</span></a>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>ニュース</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>