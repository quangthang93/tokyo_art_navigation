<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl4">
        COLUMN
        <span>シリーズ一覧</span>
      </h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-end--cnt mgb-80">
    <div class="container5">
      <div class="p-end--cnt-direct">
        <a href="/series" class="view-more">新着記事一覧へ</a>
      </div>
      <div class="p-column--cnt">
        <div class="p-column--row">
          <div class="p-end--ttlIntroWrap mgb-60">
            <p class="p-end--ttlIntro" href=""><span>連載中のシリーズ</span></p>
          </div>
          <ul class="section-column--list">
            <li class="section-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2">いまこそ語ろう、演劇のこと</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-4">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 yellow">アーティスト・サバイバル・メソッド</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-6">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 blue">東京の静寂を探しに</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column03.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 red">アーティスト解体新書</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column04.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-4">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 blue2">イベント・レポート</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-6">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 yellow2">パブリックドメインで巡る世界の美術館</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column06.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-6">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 gray">石川直樹　東京の記憶を旅する</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column07.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-6">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 green">Next Tokyo 発見隊！</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column08.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
            <li class="section-column--item fadeup delay-6">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-column--inforWrap">
                  <h3 class="ttl-column type2 gray">シリーズタイトル</h3>
                  <div class="section-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column09.png" alt="">
                    </div>
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div>
        <div class="p-column--row">
          <div class="p-end--ttlIntroWrap mgb-60">
            <p class="p-end--ttlIntro" href=""><span>アーカイブ</span></p>
          </div>
          <ul class="p-column--list">
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive01.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">溝口イタルのテアトルで会いましょう</h3>
                </div>
              </a>
            </li>
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive02.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">異分野×アーティスト</h3>
                </div>
              </a>
            </li>
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive03.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">西堂行人のトーキョー・シアター・ナビ</h3>
                </div>
              </a>
            </li>
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive04.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">松蔭先生の課外授業</h3>
                </div>
              </a>
            </li>
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive05.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">江戸アートナビ</h3>
                </div>
              </a>
            </li>
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive06.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">キュンチョメのアートデート</h3>
                </div>
              </a>
            </li>
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive07.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">アーティスト・ピックアップ</h3>
                </div>
              </a>
            </li>
            <li class="p-column--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="p-column--inforWrap">
                  <div class="p-column--item-thumbWrap">
                    <div class="img-hover-zoom">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/archive08.png" alt="">
                    </div>
                  </div>
                  <h3 class="p-column--item-ttl">アートの視点</h3>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="align-center mgt-60 fadeup delay-2">
        <a href="/column" class="view-more2"><span>コラムトップへ</span></a>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>コラム</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>