<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl5"><span>いまこそ語ろう、演劇のこと</span></h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-end--cnt mgb-80">
    <div class="container5">
      <div class="p-end--cnt-direct">
        <a href="/column/series" class="view-more">シリーズ一覧へ</a>
      </div>
      <ul class="section-column--list">
        <li class="section-column--item fadeup delay-2">
          <a href="" class="img-hover-zoomWrap">
            <div class="section-column--inforWrap">
              <div class="section-column--item-thumbWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                </div>
              </div>
              <div class="section-column--infor">
                <p class="number">No.004</p>
                <h4 class="title-lv4">劇団チョコレートケーキ（後編）</h4>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">記事を読む</span>
                </div>
              </div>
            </div>
          </a>
        </li>
        <li class="section-column--item fadeup delay-2">
          <a href="" class="img-hover-zoomWrap">
            <div class="section-column--inforWrap">
              <div class="section-column--item-thumbWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column10.png" alt="">
                </div>
              </div>
              <div class="section-column--infor">
                <p class="number">No.003</p>
                <h4 class="title-lv4">劇団チョコレートケーキ（後編）</h4>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">記事を読む</span>
                </div>
              </div>
            </div>
          </a>
        </li>
        <li class="section-column--item fadeup delay-2">
          <a href="" class="img-hover-zoomWrap">
            <div class="section-column--inforWrap">
              <div class="section-column--item-thumbWrap">
                <div class="img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column11.png" alt="">
                </div>
              </div>
              <div class="section-column--infor">
                <p class="number">No.002</p>
                <h4 class="title-lv4">劇団民藝（後編）</h4>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">記事を読む</span>
                </div>
              </div>
            </div>
          </a>
        </li>
        <li class="section-column--item fadeup delay-2">
          <a href="" class="img-hover-zoomWrap">
            <div class="section-column--inforWrap">
              <div class="section-column--item-thumbWrap">
                <div class="img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column12.png" alt="">
                </div>
              </div>
              <div class="section-column--infor">
                <p class="number">No.001</p>
                <h4 class="title-lv4">劇団チョコレートケーキ（後編）</h4>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">記事を読む</span>
                </div>
              </div>
            </div>
          </a>
        </li>
      </ul>
      <div class="align-center mgt-60 fadeup delay-2">
        <a href="" class="view-more2"><span>トップページへ</span></a>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>コラム</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>