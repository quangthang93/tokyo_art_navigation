<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl3">COLUMN</h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-end--cnt mgb-80">
    <div class="container5">
      <div class="p-end--cnt-direct">
        <a href="/column/series" class="view-more">シリーズ一覧へ</a>
      </div>
      <ul class="section-column--list">
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2">
                  <a class="view-more" href="/column/series/series-top">いまこそ語ろう、演劇のこと</a>
                  <span class="ttl-column--icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><defs><style>.a1,.b1{fill:#88e51c;}.b1{opacity:0.5;}</style></defs><g transform="translate(-20 -3541)"><path class="a1" d="M0,0,5,5V16H0Z" transform="translate(20 3541)"/><path class="b1" d="M0,5,5,0V16H0Z" transform="translate(20 3546) rotate(-90)"/></g></svg>
                  </span>
                </h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">劇団チョコレートケーキ（後編）</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2">
                  <span class="ttl-column--icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><defs><style>.a2,.b2{fill:#ccbf15;}.b2{opacity:0.5;}</style></defs><g transform="translate(-20 -3541)"><path class="a2" d="M0,0,5,5V16H0Z" transform="translate(20 3541)"/><path class="b2" d="M0,5,5,0V16H0Z" transform="translate(20 3546) rotate(-90)"/></g></svg>
                  </span>
                  <a class="view-more" href="/column/series/series-top">アーティスト・サバイバル・メソッド</a>
                </h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">アーティストの仕事図鑑 #1　滝戸ドリタ</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column03.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">

                <h3 class="ttl-column type2">
                  <a class="view-more" href="/column/series/series-top">東京の静寂を探しに</a>
                  <span class="ttl-column--icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><defs><style>.a3,.b3{fill:#6ba0c3;}.b3{opacity:0.5;}</style></defs><g transform="translate(-20 -3541)"><path class="a3" d="M0,0,5,5V16H0Z" transform="translate(20 3541)"/><path class="b3" d="M0,5,5,0V16H0Z" transform="translate(20 3546) rotate(-90)"/></g></svg>
                  </span>
                </h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">小石川後楽園＜後編＞</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column04.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2">
                  <a class="view-more" href="/column/series/series-top">アーティスト解体新書</a>
                  <span class="ttl-column--icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><defs><style>.a4,.b4{fill:#e68b8b;}.b4{opacity:0.5;}</style></defs><g transform="translate(-20 -3541)"><path class="a4" d="M0,0,5,5V16H0Z" transform="translate(20 3541)"/><path class="b4" d="M0,5,5,0V16H0Z" transform="translate(20 3546) rotate(-90)"/></g></svg>
                  </span>
                </h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">ラファエロ・サンティ</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2">
                  <a class="view-more" href="/column/series/series-top">イベント・レポート</a>
                  <span class="ttl-column--icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><defs><style>.a5,.b5{fill:#1cd5e5;}.b5{opacity:0.5;}</style></defs><g transform="translate(-20 -3541)"><path class="a5" d="M0,0,5,5V16H0Z" transform="translate(20 3541)"/><path class="b5" d="M0,5,5,0V16H0Z" transform="translate(20 3546) rotate(-90)"/></g></svg>
                  </span>
                </h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">伝説のアパートを「記憶」をもとに再現「豊島区立トキワ荘マンガミュージアム」</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column purple">Pick up</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">

                <h3 class="ttl-column type2">
                  <a class="view-more" href="/column/series/series-top">もう一度読みたい！人気コラム</a>
                  <span class="ttl-column--icon">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"><defs><style>.a6,.b6{fill:#c19fe8;}.b6{opacity:0.5;}</style></defs><g transform="translate(-20 -3541)"><path class="a6" d="M0,0,5,5V16H0Z" transform="translate(20 3541)"/><path class="b6" d="M0,5,5,0V16H0Z" transform="translate(20 3546) rotate(-90)"/></g></svg>
                  </span>
                </h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">小石川後楽園＜後編＞</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
      </ul>
      <div class="pagination">
        <p class="pagination-label">50件中｜1〜20件 表示</p>
        <div class="pagination-list">
          <a class="ctrl prev" href="">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"><path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(-717 4092) rotate(90)"/></svg>
          </a>
          <a class="active" href="">1</a>
          <a href="">2</a>
          <a href="">3</a>
          <a href="">4</a>
          <a href="">5</a>
          <a href="">6</a>
          <div class="pagination-spacer">…</div>
          <a href="">12</a>
          <a class="ctrl next" href="">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10"><path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(723 -4082) rotate(-90)"/></svg>
          </a>
        </div>
      </div>
      <div class="align-center mgt-60 fadeup delay-2">
        <a href="" class="view-more2"><span>トップページへ</span></a>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>コラム</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>