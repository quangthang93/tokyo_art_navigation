<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5">
      <h1 class="p-end--ttlCategory">美術</h1>
    </div>
  </section><!-- ./p-recruit--banner -->
  <div class="p-end--cnt mgb-80">
    <div class="container5">
      <ul class="section-column--list">
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2"><a class="view-more" href="/column/series/series-top">いまこそ語ろう、演劇のこと</a></h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">劇団チョコレートケーキ（後編）</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2 yellow"><a class="view-more" href="/column/series/series-top">アーティスト・サバイバル・メソッド</a></h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">アーティストの仕事図鑑 #1　滝戸ドリタ</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column03.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2 blue"><a class="view-more" href="/column/series/series-top">東京の静寂を探しに</a></h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">小石川後楽園＜後編＞</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column04.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2 red"><a class="view-more" href="/column/series/series-top">アーティスト解体新書</a></h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">ラファエロ・サンティ</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column new">NEW</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2 blue2"><a class="view-more" href="/column/series/series-top">イベント・レポート</a></h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">伝説のアパートを「記憶」をもとに再現「豊島区立トキワ荘マンガミュージアム」</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
        <li class="section-column--item fadeup">
          <div class="section-column--item-inner">
            <div class="section-column--inforWrap">
              <a href="/column/detail" class="section-column--item-thumbWrap img-hover-zoomWrap">
                <div class="img-hover-zoom relative">
                  <span class="label-column purple">Pick up</span>
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                </div>
              </a>
              <div class="section-column--infor">
                <h3 class="ttl-column type2 purple"><a class="view-more" href="/column/series/series-top">もう一度読みたい！人気コラム</a></h3>
                <a href="/column/detail" class="section-column--infor-link link2">
                  
                  <h4 class="title-lv4">小石川後楽園＜後編＞</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">記事を読む</span>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </li>
      </ul>
      <div class="pagination">
        <p class="pagination-label">50件中｜1〜20件 表示</p>
        <div class="pagination-list">
          <a class="ctrl prev" href="">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
              <path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(-717 4092) rotate(90)" /></svg>
          </a>
          <a class="active" href="">1</a>
          <a href="">2</a>
          <a href="">3</a>
          <a href="">4</a>
          <a href="">5</a>
          <a href="">6</a>
          <div class="pagination-spacer">…</div>
          <a href="">12</a>
          <a class="ctrl next" href="">
            <svg xmlns="http://www.w3.org/2000/svg" width="6" height="10" viewBox="0 0 6 10">
              <path class="a" d="M-4087-717l-5-4.8,1.25-1.2,3.75,3.6,3.75-3.6,1.25,1.2Z" transform="translate(723 -4082) rotate(-90)" /></svg>
          </a>
        </div>
      </div>
      <div class="align-center mgt-60 fadeup delay-2">
        <a href="" class="view-more2"><span>トップページへ</span></a>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li><a href="/">コラム</a></li>
        <li>カテゴリ（美術）</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>