<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <div class="p-column--detail">
    <div class="p-column--detail-head align-center">
      <h1 class="p-end--ttl2">劇団チョコレートケーキ（前編）</h1>
      <div class="p-column--detail-subTtlWrap">
        <p class="ttl-line-green"><a href="/column/series/series-top">いまこそ語ろう、演劇のこと</a></p>
        <span class="p-column--detail-number number2">No.003</span>
      </div>
    </div><!-- ./p-column--detail-head -->
    <div class="p-column--detail-img">
      <!-- <img src="<?php echo $PATH;?>/assets/images/end/column/column01.png" alt=""> -->
      <img class="column_thumnail_tate" src="<?php echo $PATH;?>/assets/images/end/column/column01.jpg" alt="">
    </div>
    <div class="p-column--detail-inner">
      <div class="p-column--detail-cnt">
        <div class="p-column--detail-infor">
          <p class="p-column--detail-infor-desc desc3">コロナ禍で混乱が続く演劇界。従来の観劇スタイルの復活がいまだ難しい状況下、劇団チョコレートケーキでも、劇場配信、アクターカメラの導入など、舞台を観客に届けるための模索が続けられている。本編では、2月の次回公演『帰還不能点』について、前編に続き、Zoom によるオンライン鼎談でさまざまな想いが語られる。</p>
          <p class="p-column--detail-infor-desc2 desc2">杉原環樹（主宰／演出家／俳優）・豊島望（俳優）・宮原朋之（俳優）</p>
          <div class="p-column--detail-infor-label">
            <div class="p-column--detail-infor-tagWrap">
              <a href="/column/categories" class="tag type3">美術</a>
              <a href="/column/categories" class="tag type3">劇団</a>
              <a href="/column/categories" class="tag type3">帰還不能点</a>
            </div>
            <div class="p-column--detail-infor-snsWrap">
              <div class="p-column--detail-infor-sns">
                <span class="p-column--detail-infor-sns-ttl">Share</span>
                <div class="p-column--detail-infor-sns-icons">
                  <a href="" class="link fb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook2.svg" alt="">
                  </a>
                  <a href="" class="link">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter2.svg" alt="">
                  </a>
                  <a href="" class="link">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-line.svg" alt="">
                  </a>
                </div>
              </div>
              <div>
                <span class="vertical-line"></span>
                <span class="date">2021.04.21</span>
              </div>
            </div>
          </div>
          <p class="ttl-bold mgt-30">劇団チョコレートケーキ［げきだんチョコレートケーキ］</p>
          <div class="p-column--detail-infor-cnt">
            <div class="p-column--detail-infor-inner">
              <p class="desc mgb-15">2000年結成。コメディを中心に発表していたが、2009年より古川健が脚本を、2010年より日澤雄介が演出を担当して以降、現在のスタイルを確立。あさま山荘事件、大逆事件、ナチスなど歴史的な事象をモチーフにした作品をつくり続けている。2014年、大正天皇の一代記を描いた『治天ノ君』で、第21回読売演劇大賞選考委員特別賞を、2015年、劇団としての実績が評価され第49回紀伊國屋演劇賞団体賞を受賞。現在、日澤雄介(演出／俳優)を代表に、古川健(劇作家／俳優)、岡本篤(俳優)、浅井伸治(俳優)、西尾友樹(俳優)、菅野佐知子(制作／俳優)の６名が所属している。</p>
              <div class="mgb-5">
                <a href="" class="link-icon blank">劇団チョコレートケーキ OFFICIAL SITE</a>
              </div>
              <div class="mgb-5">
                <a href="" class="link-icon">劇団チョコレートケーキ OFFICIAL SITE</a>
              </div>
            </div>
            <div class="p-column--detail-infor-thumbWrap">
              <div class="p-column--detail-infor-thumb">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/end/detail02.png" alt="">
              </div>
            </div>
          </div>
          <div>
            <div class="p-column--detail-infor-row">
              <h3 class="ttl-border-left mgb-15">歴史的な題材を手がけること</h3>
              <p class="desc mgb-15">2000年結成。コメディを中心に発表していたが、2009年より古川健が脚本を、2010年より日澤雄介が演出を担当して以降、現在のスタイルを確立。あさま山荘事件、大逆事件、ナチスなど歴史的な事象をモチーフにした作品をつくり続けている。2014年、大正天皇の一代記を描いた『治天ノ君』で、第21回読売演劇大賞選考委員特別賞を、2015年、劇団としての実績が評価され第49回紀伊國屋演劇賞団体賞を受賞。現在、日澤雄介(演出／俳優)を代表に、古川健(劇作家／俳優)、岡本篤(俳優)、浅井伸治(俳優)、西尾友樹(俳優)、菅野佐知子(制作／俳優)の６名が所属している。</p>
              <div class="mgb-40">
                <div class="align-center">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/column02.png" alt="">
                </div>
                <p class="desc2 mgt-5">左より林竜三、西尾友樹</p>
              </div>
              <p class="txt-list mgt-30 mgb-15">普段はあまり、アートを見られないそうですね。</p>
              <p class="desc">pha：そうですね。島そのものに興味があって、アート観光で有名な直島に行ったこともあるんですけど、美術館は見なかった（笑）。でも、誘われて見に行くと、好きな作品もあるんです。実際、Mr.さんは好きで、知っていました。<br><br>というのも、一度ギークハウス（本文リンク）で「カオス*ラウンジ」というグループの展示に誘ってもらったことがあって。それは会場で、みんなでいつも通り寝転んでパソコンをいじるものだったのですが、見た人からMr.さんの名前を聞いたんです。それで調べてみると、「なるほどな」と。 <br><br>Mr.：僕もそのころですよ、ギークハウスの存在を知ったのは。</p>
              <div class="list">
                <p class="ttl-bold mgb-10">参加アーティスト</p>
                <ul>
                  <li>アイ・ウェイウェイ</li>
                  <li>ブルームバーグ＆チャナリン</li>
                  <li>マウリツィオ・カテラン</li>
                  <li>ドン・ユアン</li>
                </ul>
              </div>
              <p class="iframe mgt-30">
                <iframe width="900" height="506" src="https://www.youtube.com/embed/6pxRHBw-k8M" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </p>
              <div class="mgt-30">
                <div class="col2-2">
                  <div class="col2-2-item">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/column02.png" alt="">
                    <p class="desc2 mgt-10">左より林竜三、西尾友樹</p>
                  </div>
                  <div class="col2-2-item">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/end/column/column02.png" alt="">
                  </div>
                </div>
              </div>
              <div class="mgt-30 mgb-40 align-center">
                <a href="" class="view-more">シリーズ一覧へ</a>
              </div>
              <div class="mgt-30 mgb-40 align-center">
                <a href="" class="view-more3">シリーズ一覧へ</a>
              </div>
            </div>
            <div class="p-column--detail-infor-row">
              <h2 class="section-title-ep">インフォメーション</h2>
              <div class="p-column--detail-infor-rowInner">
                <p class="ttl-bold">小石川後楽園</p>
                <div class="p-column--detail-infor-cnt">
                  <div class="p-column--detail-infor-inner">
                    <p class="desc mgb-15">2000年結成。コメディを中心に発表していたが、2009年より古川健が脚本を、2010年より日澤雄介が演出を担当して以降、現在のスタイルを確立。あさま山荘事件、大逆事件、ナチスなど歴史的な事象をモチーフにした作品をつくり続けている。2014年、大正天皇の一代記を描いた『治天ノ君』で、第21回読売演劇大賞選考委員特別賞を、2015年、劇団としての実績が評価され第49回紀伊國屋演劇賞団体賞を受賞。現在、日澤雄介(演出／俳優)を代表に、古川健(劇作家／俳優)、岡本篤(俳優)、浅井伸治(俳優)、西尾友樹(俳優)、菅野佐知子(制作／俳優)の６名が所属している。</p>
                  </div>
                  <div class="p-column--detail-infor-thumbWrap">
                    <div class="p-column--detail-infor-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/end/detail02.png" alt="">
                    </div>
                  </div>
                </div>
                <div class="table">
                  <table>
                    <tr>
                      <th>所在地</th>
                      <td>東京都文京区後楽一丁目6-6 <a class="link-blue" href="https://goo.gl/maps/FAMooFdM99amzeA36" target="_blank">[MAP]</a></td>
                    </tr>
                    <tr>
                      <th>開園時間</th>
                      <td>9:00-17:00（入館は16:30まで）</td>
                    </tr>
                    <tr>
                      <th>休園日</th>
                      <td>年末年始</td>
                    </tr>
                    <tr>
                      <th>入園料</th>
                      <td>一般300円、65歳以上150円</td>
                    </tr>
                    <tr>
                      <th>URL</th>
                      <td><a href="https://tokyoartnavi.jp/talkplay/index004.php" class="link-icon blank type2" target="_blank">https://tokyoartnavi.jp/talkplay/index004.php</a></td>
                    </tr>
                    <tr>
                      <th>備考</th>
                      <td>【開演時間・客席数変更のお知らせ】<br> 1月7日「緊急事態宣言」の発令に伴い、時短要請と客席削減要請を受け2月19日(金)～28日(日)の東京芸術劇場 シアターイーストの公演を次のように変更いたします。</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div><!-- ./p-column--detail-infor -->
        <div class="p-column--detail-sidebar">
          <div class="p-column--detail-sidebar-listGroup">
            <div class="p-column--detail-sidebar-row">
              <p class="p-column--detail-sidebar-ttl">バックナンバー</p>
              <p class="ttl-line-green"><a href="/column/series/series-top">いまこそ語ろう、演劇のこと</a></p>
              <ul class="p-column--detail-sidebar-list">
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団チョコレートケーキ（後編）</p>
                  </a>
                </li>
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団チョコレートケーキ（前編）</p>
                  </a>
                </li>
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column11.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団民藝（後編）</p>
                  </a>
                </li>
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column12.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団民藝（前編）</p>
                  </a>
                </li>
              </ul>
              <div>
                <a href="" class="btn-viewmore">もっと見る</a>
              </div>
            </div>
            <div class="p-column--detail-sidebar-row">
              <p class="p-column--detail-sidebar-ttl">新着記事</p>
              <ul class="p-column--detail-sidebar-list">
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団チョコレートケーキ（後編）</p>
                  </a>
                </li>
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団チョコレートケーキ（前編）</p>
                  </a>
                </li>
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column03.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団民藝（後編）</p>
                  </a>
                </li>
                <li class="p-column--detail-sidebar-item link">
                  <a href="">
                    <div class="p-column--detail-sidebar-item-thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/column04.png" alt="">
                    </div>
                    <p class="p-column--detail-sidebar-item-desc">劇団民藝（前編）</p>
                  </a>
                </li>
              </ul>
              <div>
                <a href="" class="btn-viewmore">もっと見る</a>
              </div>
            </div>
          </div>
        </div><!-- ./p-column--detail-sidebar -->
      </div>
    </div><!-- ./p-column--detail-inner -->
  </div><!-- ./p-column--detail -->
  <div class="align-center fadeup delay-2">
    <a href="/exhibition-event/" class="view-more2"><span>展覧会・イベント情報</span></a>
  </div>
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li><a href="/">コラム</a></li>
        <li><a href="/">シリーズ一覧</a></li>
        <li><a href="/">いまこそ語ろう、演劇のこと</a></li>
        <li>劇団チョコレートケーキ（前編）</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>