jQuery(function($) {
	var $btnViewmore = $(".js-btnViewmore");
	var $listInsta = $(".js-listInsta");
	var $loading = $(".js-loading");
	var $html = '<li class="col4-2-item"><a href="" class="img-hover-zoomWrap"><div class="section-artists--item-thumb img-hover-zoom"><img class="js-instaImg cover" src="/assets/images/top/artists01.png" alt=""></div><p class="section-artists--item-ttl">@ Tamabi_member</p></a></li><li class="col4-2-item"><a href="" class="img-hover-zoomWrap"><div class="section-artists--item-thumb img-hover-zoom"><img class="js-instaImg cover" src="/assets/images/top/artists02.png" alt=""></div><p class="section-artists--item-ttl">@ Tamabi_member</p></a></li><li class="col4-2-item"><a href="" class="img-hover-zoomWrap"><div class="section-artists--item-thumb img-hover-zoom"><img class="js-instaImg cover" src="/assets/images/top/artists03.png" alt=""></div><p class="section-artists--item-ttl">@ Tamabi_member</p></a></li><li class="col4-2-item"><a href="" class="img-hover-zoomWrap"><div class="section-artists--item-thumb img-hover-zoom"><img class="js-instaImg cover" src="/assets/images/top/artists04.png" alt=""></div><p class="section-artists--item-ttl">@ Tamabi_member</p></a></li>';
	
	$loading.hide();
	$btnViewmore.click(function () {
		$(this).hide();
		$loading.show();
		setTimeout(function () {
			$listInsta.append($html);
		}, 500);
		setTimeout(function () {
			$btnViewmore.show();
			$loading.hide();
		}, 505);
	});
});