// $(function() {
jQuery(function($) {

  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
      breakpointTB = 1050,
      wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href= $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top - 100;
      $('body,html').stop().animate({scrollTop:position}, 1000);
      return false;
    });
  }

  // Animation scroll to top
  var clickPageTop = function() {
    var $pageTop = $('.js-pageTop');
    $pageTop.click(function(e) {
      $('html,body').animate({ scrollTop: 0 }, 300);
    });
  }

  // Animation on scroll
  var scrollLoad = function() {
   var scroll = $(this).scrollTop();
    $('.fadeup, .fadein, .fadedown').each(function() {
      var elemPos = $(this).offset().top;
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 100) {
        $(this).addClass('in');
      }
    });
  }

  // Trigger Pagetop
  var triggerSupport = function() {

    wWindow = $(window).outerWidth();
    var $iconSupport = $('.js-support');
    var $iconSupportPoint = $('.js-supportPoint');

    if ($iconSupport.length && $iconSupportPoint.length && wWindow > 1050) {
      var $posSupport = $iconSupport.offset().top;
      var $posSupportPoint = $iconSupportPoint.offset().top;
      if ($posSupport >= $posSupportPoint + 200) {
        $iconSupport.css("z-index", -9999);
      } else {
        $iconSupport.css("z-index", 1);
      }
    }
  }  


  // Trigger Accordion
  var triggerAccordion = function() {
    var $accorLabel = $('.js-accorLabel'),
        $accorCnt = $('.js-accorCnt');
        $accorCntWrap = $('.js-accorCntWrap');

    $accorLabel.click(function () {
      var btnVal = ($(this).hasClass('active')) ? "コンテンツ紹介" : "閉じる";
      $(this).toggleClass('active').html(btnVal).parent().siblings('.js-accorCntWrap').find('.js-accorCnt').toggleClass('active').slideToggle();
    });   
  }  

  // Tabs Control
  var tabsControl = function() {
    var $tabsNav = $('.js-tabsNav'),
        $tabsItem = $('.js-tabsItem'),
        $tabsCnt = $('.js-tabsCnt'),
        $tabsPanel = $('.js-tabsPanel');

    // Setting first view
    $tabsPanel.hide();
    $tabsCnt.each(function () {
        $(this).find($tabsPanel).eq(0).show();
    });
    $tabsNav.each(function () {
        $(this).find($tabsItem).eq(0).addClass('active');
    });

    // Click event
    $tabsItem.on('click', function () {
      var tMenu = $(this).parents($tabsNav).find($tabsItem);
      var tCont = $(this).parents($tabsNav).siblings($tabsCnt).find($tabsPanel);
      var index = tMenu.index(this);
      tMenu.removeClass('active');
      $(this).addClass('active');
      tCont.hide();
      tCont.eq(index).show();
    });
  } 


  // Slider init
  // Slider (Restaurant detail page)
  var initSliders = function() {
    var $slide01 = $('.js-slider-top'); 
    var $slide02 = $('.js-slider-artists'); 
    var $slide04 = $('.js-slider-detail'); 

    // Slider 01
    if ($slide01.length) {
      // Tabs slider
      $slide01.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        swipeToSlide: true,
        // fade: true,
        dots: true,
        prevArrow: '<a class="slick-prev"><span class="top"></span></a>',
        nextArrow: '<a class="slick-next"><span class="top"></span></a>',
        // responsive: [{
        //   breakpoint: 768,
        //   settings: {
        //     slidesToShow: 1
        //   }
        // }]
      });
    }

    // Slider 02
    if ($slide02.length) {
      $status2 = $('.js-pagingInfo');
      $slide02.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $status2.html('<span>'+i+'</span>' + '<i> </i>' + slick.slideCount);
      });

      // Tabs slider
      $slide02.slick({
        // rows: 1,
        slidesPerRow: 6,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        swipeToSlide: true,
        // fade: true,
        dots: false,
        prevArrow: '<a class="slick-prev"><span></span></a>',
        nextArrow: '<a class="slick-next"><span></span></a>',
        responsive: [{
          breakpoint: 768,
          settings: {
            rows: 2,
            slidesPerRow: 2,
            slidesToShow: 2,
            slidesToScroll: 4,
            dots: false,
            arrows: true,
            infinite: true,
            speed: 300,
          }
        }]
      });
    }


    // Slider 03
    if ($slide04.length) {
      // Tabs slider
      $slide04.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        swipeToSlide: true,
        // fade: true,
        dots: false,
        prevArrow: '<a class="slick-prev type2"><span></span></a>',
        nextArrow: '<a class="slick-next type2"><span></span></a>',
      });
    }
  }

  //match height
  var matchHeight = function() {
    var $elem01 = $('.ttl-column');
    var $elem02 = $('.section-contest--item-inner');
    var $elem03 = $('.p-news--detail-slider li a');
    var $elem04 = $('.p-exhibiton--detail-slider li a');
    var $elem05 = $('.js-instaImg');
    if ($elem05.length) {
      $elem05.matchHeight();
    }
    if ($elem01.length) {
      $elem01.matchHeight();
    }
    if ($elem02.length) {
      $elem02.matchHeight();
    }
    if ($elem03.length) {
      $elem03.matchHeight();
    }
    if ($elem04.length) {
      $elem04.matchHeight();
    }
  }

  // Trigger background for header
  var triggerBackground = function() {
    if ( wWindow <= 1050) {
      if ($(this).scrollTop() > 100) {
        $('.js-header').addClass('bg-white');
        $('.js-logo01, .js-logo02, .js-menu-list, .js-menuTrigger').addClass('active');
      } else {
        $('.js-header').removeClass('bg-white');
        $('.js-logo01, .js-logo02, .js-menu-list, .js-menuTrigger').removeClass('active');
      }
    }

    if ( wWindow > 1050) {
      if ($(this).scrollTop() > 100) {
        $('.js-menuTrigger2').addClass('show');
        $('.js-menu').hide();
        $('.js-menu2').show();
      } else {
        $('.js-menuTrigger2').removeClass('show');
        $('.js-menu').show();
        $('.js-menu2').hide();

        $('.js-header').removeClass('active');
      }

    } 
  }

  var triggerMenu = function () {
    if ( wWindow > 1050) {
        $('.js-menuTrigger2').click(function () {
          $('.js-menu').hide();
          $('.js-header').toggleClass('active');
        });
    }
  }


  // Date picker init
  var initDatePicker = function() {
    $.datepicker.setDefaults($.datepicker.regional["ja"]);
    var $datePicker01 = $('.js-datepicker01');
    if ($datePicker01.length) {
      $datePicker01.datepicker({
        firstDay: 1,
        dateFormat: 'yy-mm-dd'
      });
    }
    var $datePicker02 = $('.js-datepicker02');
    if ($datePicker02.length) {
      $datePicker02.datepicker({
        firstDay: 1,
        minDate:3
      });
    }
  }

  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    matchHeight();
    anchorLink();
    scrollLoad();
    objectFitImages();
    clickPageTop();
    triggerAccordion();
    tabsControl();
    initSliders();
    triggerBackground();
    triggerMenu();
    initDatePicker();
    triggerSupport();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  $(window).resize(function() {
    wWindow = $(window).outerWidth();
  });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  $(window).scroll(function() {
    scrollLoad();
    triggerSupport();
    triggerBackground();
  });

});