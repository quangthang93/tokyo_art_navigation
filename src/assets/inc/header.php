<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Tokyo Art Navigation</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <!-- <link rel="shortcut icon" href="assets/images/favicon.png">
  <link rel="apple-touch-icon" href="assets/images/favicon.png"> -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Barlow:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header fadedown js-header">
      <h1 class="header-logo link">
        <a href="" class="link">
          <img class="js-logo01" src="<?php echo $PATH;?>/assets/images/common/hlogo.svg" alt="東京のアートシーンを発信し、創造しよう。">
        </a>
      </h1>
      <div class="header-menu js-menu">
        <ul class="js-menu-list">
          <li><a class="link" href="">コラム</a></li>
          <li><a class="link" href="">コンテスト情報</a></li>
          <li><a class="link" href="">展覧会・イベント情報</a></li>
          <li><a class="link" href="">支援情報</a></li>
          <li><a class="link" href="">TOKYO ARTIST</a></li>
          <li><a class="link" href="">ご利用ガイド</a></li>
        </ul>
        <div class="header-sns">
          <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt=""></a>
          <!-- <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt=""></a> -->
          <a class="link" href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt=""></a>
        </div>
      </div>
      <div class="header-menu type2 pc-only js-menu2">
        <ul class="js-menu-list type2">
          <li><a class="link" href="">コラム</a></li>
          <li><a class="link" href="">コンテスト情報</a></li>
          <li><a class="link" href="">展覧会・イベント情報</a></li>
          <li><a class="link" href="">支援情報</a></li>
          <li><a class="link" href="">TOKYO ARTIST</a></li>
          <li><a class="link" href="">ご利用ガイド</a></li>
        </ul>
      </div>
      <a class="header-ctrl js-menuTrigger sp-only" href="javascript:void(0);">
        <span></span>
        <span>MENU</span>
        <span></span>
      </a>
    </header><!-- ./header -->
    <a class="header-ctrl type2 js-menuTrigger2 pc-only" href="javascript:void(0);">
      <span></span>
      <span>MENU</span>
      <span></span>
    </a>