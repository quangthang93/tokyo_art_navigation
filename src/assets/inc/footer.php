		<footer class="footer">      
      <div class="container">
        <div class="footer-inner">
<!--           <span class="footer-inner--corner corner01"></span>
          <span class="footer-inner--corner corner02"></span>
          <span class="footer-inner--corner corner03"></span>
          <span class="footer-inner--corner corner04"></span> -->
        
          <div class="footer-nav">
            <div class="footer-nav01">
              <div class="footer-nav01--logo">
                <a href="" class="link">
                  <img src="<?php echo $PATH;?>/assets/images/common/flogo.svg" alt="Tokyo art navigation">
                  <!-- <img src="<?php echo $PATH;?>/assets/images/common/hlogo01.svg" alt="東京のアートシーンを発信し、創造しよう。"> -->
                </a>
              </div>
              <div class="footer-nav01--link">
                <ul>
                  <li><a class="link" href="">コラム</a></li>
                  <li><a class="link" href="">コンテスト情報</a></li>
                  <li><a class="link" href="">展覧会・イベント情報</a></li>
                  <li><a class="link" href="">支援情報</a></li>
                  <li><a class="link" href="">TOKYO ARTIST</a></li>
                  <li><a class="link" href="">ご利用ガイド</a></li>
                </ul>
              </div>    
            </div>
            <div class="footer-nav02">
              <ul>
                <li><a class="link" href="">ご利用ガイド</a></li>
                <li><a class="link" href="">情報掲載をご希望の方</a></li>
                <li><a class="link" href="">ニュース</a></li>
                <li><a class="link" href="">お問い合わせ</a></li>
                <li><a class="link" href="">リンク集</a></li>
                <li><a class="link" href="">サイトマップ</a></li>
                <li><a class="link" href="">ご利用規約</a></li>
                <li><a class="link" href="">ウェブアクセシビリティ</a></li>
                <li><a class="link" href="">プライバシーポリシー</a></li>
              </ul>
            </div>
          </div>
          <div class="footer-banner">
            <ul>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner01.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner02.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner03.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner04.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner05.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner06.png" alt="">
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="footer-copyWrap">
          <div class="footer-copy">
            <a href="" class="footer-copy--logo">
              <img src="<?php echo $PATH;?>/assets/images/common/logo-copy.png" alt="">
            </a>
            <p class="footer-copy--txt">Copyright © Tokyo Metropolitan Foundation for History and Culture.All rights reserved. All rights reserved.<br>
          掲載画像・その他の無断転載・転用を禁じます。</p>
          </div>
          <p class="footer-copy--txt mgt-25">Tokyo Art Navifationは公益財団法人東京都歴史文化財団が運営しています。</p>
        </div>
      </div>

    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-ui.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/top.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/instagram.js"></script>
</body>

</html>