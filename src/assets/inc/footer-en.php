		<footer class="footer">      
      <div class="container">
        <div class="footer-inner">
          <!-- <span class="footer-inner--corner corner01"></span>
          <span class="footer-inner--corner corner02"></span>
          <span class="footer-inner--corner corner03"></span>
          <span class="footer-inner--corner corner04"></span> -->
        
          <div class="footer-nav">
            <div class="footer-nav01">
              <div class="footer-nav01--logo">
                <a href="" class="link">
                  <img src="<?php echo $PATH;?>/assets/images/common/flogo.svg" alt="Tokyo art navigation">
                  <!-- <img src="<?php echo $PATH;?>/assets/images/common/hlogo01.svg" alt="東京のアートシーンを発信し、創造しよう。"> -->
                </a>
              </div>
              <div class="footer-nav01--link">
                <ul>
                  <li><a class="link" href="">COLUMN</a></li>
                  <li><a class="link" href="">TOKYO ARTISTS</a></li>
                </ul>
              </div>    
            </div>
            <div class="footer-nav02">
              <ul>
                <li><a class="link" href="">NEWS</a></li>
                <li><a class="link" href="">RELATED LINKS</a></li>
                <li><a class="link" href="">SITE POLICY</a></li>
                <li><a class="link" href="">PRIVACY POLICY</a></li>
              </ul>
            </div>
          </div>
          <div class="footer-banner">
            <ul>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner01.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner02.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner03.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner04.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner05.png" alt="">
                </a>
              </li>
              <li class="img-hover-zoomWrap">
                <a class="img-hover-zoom" href="" target="_blank">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-banner06.png" alt="">
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class="footer-copyWrap">
          <div class="footer-copy">
            <a href="" class="footer-copy--logo">
              <img src="<?php echo $PATH;?>/assets/images/common/logo-copy-en.png" alt="">
            </a>
            <p class="footer-copy--txt">Copyright © Tokyo Metropolitan Foundation for History and Culture.All rights reserved. All rights reserved.</p>
          </div>
        </div>
      </div>

    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-ui.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/top.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/instagram.js"></script>
</body>

</html>