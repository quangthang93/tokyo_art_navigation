<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="main-mv">
    <div class="container fadein">
      <ul class="main-mv--slider js-slider-top">
        <li class="main-mv--slider-item">
          <a href="" class="main-mv--slider-itemInner link">
            <div class="main-mv--slider-thumbWrap">
              <div class="main-mv--slider-thumb">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/top/mv-slider01.png" alt="">
              </div>
            </div>
            <div class="main-mv--slider-cnt">
              <div class="main-mv--slider-cntInner">
                <div class="main-mv--slider-label">
                  <span class="label">イベントレポート</span>
                </div>
                <p class="ttl-bold">子供も大人も思いっきり遊ぼう！</p>
                <h3 class="main-mv--slider-ttl ttl-blue"><span>PLAY! MUSEUM PLAY! PARK</span></h3>
                <p class="desc">合言葉は「未知との出会い」。白くて柔らかい〈大きなお皿〉や〈ファクトリー〉など7つのエリアで、身体を使った遊びやワークショップを楽しもう。</p>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">読む</span>
                </div>
              </div>
            </div>
          </a>
        </li>
        <li class="main-mv--slider-item">
          <a href="" class="main-mv--slider-itemInner link">
            <div class="main-mv--slider-thumbWrap">
              <div class="main-mv--slider-thumb">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/top/mv-slider01.png" alt="">
              </div>
            </div>
            <div class="main-mv--slider-cnt">
              <div class="main-mv--slider-cntInner">
                <div class="main-mv--slider-label">
                  <span class="label">イベントレポート</span>
                </div>
                <p class="ttl-bold">子供も大人も思いっきり遊ぼう！</p>
                <h3 class="main-mv--slider-ttl ttl-blue"><span>PLAY! MUSEUM PLAY! PARK</span></h3>
                <p class="desc">合言葉は「未知との出会い」。白くて柔らかい〈大きなお皿〉や〈ファクトリー〉など7つのエリアで、身体を使った遊びやワークショップを楽しもう。</p>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">読む</span>
                </div>
              </div>
            </div>
          </a>
        </li>
      </ul><!-- ./main-mv--slider -->
      <div class="main-mv--news">
        <p class="main-mv--news-ttl">NEWS</p>
        <a href="" class="main-mv--news-cnt link">
          <p class="date">2021.04.25</p>
          <p class="desc">システムメンテナンスのお知らせ</p>
        </a>
      </div>
      <p class="main-mv--notices">新型コロナウイルス感染拡大防止のため、各イベントは中止・変更になる場合があります。最新情報は各イベントの公式サイトでご確認ください。</p>
    </div><!-- ./container -->
  </div><!-- ./main-mv -->

  <section class="section-topics">
    <h2 class="section-ttl fadeup">
      <span>TOPICS</span>
      東京の“イマ”を知る
    </h2>
    <div class="section-topics--head">
      <div class="container">
        <ul class="section-topics--head-list">
          <li class="section-topics--head-item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-topics--head-item-thumb img-hover-zoom">
                <img src="<?php echo $PATH;?>/assets/images/top/topics01.png" alt="">
              </div>
              <div class="section-topics--head-item-infor">
                <div class="section-topics--head-item-tags">
                  <span class="tag">参加イベント</span>
                </div>
                <p class="desc">東京都美術館「没後70年 吉田博展」展覧会チケットプレゼントキャンペーン！</p>
              </div>
            </a>
          </li>
          <li class="section-topics--head-item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-topics--head-item-thumb img-hover-zoom">
                <img src="<?php echo $PATH;?>/assets/images/top/topics02.png" alt="">
              </div>
              <div class="section-topics--head-item-infor">
                <div class="section-topics--head-item-tags">
                  <span class="tag">参加イベント</span>
                </div>
                <p class="desc">東京都美術館「没後70年 吉田博展」展覧会チケットプレゼントキャンペーン！</p>
              </div>
            </a>
          </li>
          <li class="section-topics--head-item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-topics--head-item-thumb img-hover-zoom">
                <img src="<?php echo $PATH;?>/assets/images/top/topics03.png" alt="">
              </div>
              <div class="section-topics--head-item-infor">
                <div class="section-topics--head-item-tags">
                  <span class="tag">参加イベント</span>
                </div>
                <p class="desc">東京都美術館「没後70年 吉田博展」展覧会チケットプレゼントキャンペーン！</p>
              </div>
            </a>
          </li>
        </ul>
        <div class="align-center fadeup">
          <a href="" class="view-more2"><span>トピックス一覧へ</span></a>
        </div>
      </div>
    </div><!-- ./section-topics--head -->

    <div class="section-topics--navi">
      <div class="section-topics--navi-top">
        <div class="section-topics--navi-sloganWrap">
          <div class="fadeup">
            <!-- <p class="section-topics--navi-slogan">
              <img class="pc-only" src="<?php echo $PATH;?>/assets/images/top/topics-slogan.svg" alt="">
              <img class="sp-only" src="<?php echo $PATH;?>/assets/images/top/topics-slogan-sp.svg" alt="">
            </p> -->
            <div class="section-topics--navi-logo">
              <img src="<?php echo $PATH;?>/assets/images/common/logo03.svg" alt="">
            </div>
          </div>
          <p class="section-topics--navi-msg fadeup">
            <!-- <span>みんなで創ろう、育てよう。</span> -->
            <span>
              <img src="<?php echo $PATH;?>/assets/images/top/navi-msg.svg" alt="">
            </span>
          </p>
        </div>
        <!-- <p class="section-topics--navi-msg ttl-bold fadeup">東京のアートシーンをナビゲート！ イベント情報やアーティスト紹介、充実のコラムが満載！</p> -->
      </div><!-- ./section-topics--navi-top -->
      <ul class="section-topics--navi-list">
        <li class="section-topics--navi-item yellow fadeup">
          <a class="link" href="">
            <div class="_icon">
              <img src="<?php echo $PATH;?>/assets/images/top/icon-navi01.svg" alt="">
            </div>
            <div class="_infor">
              アート情報を知りたい！
              <span>一般ユーザー</span>
            </div>
          </a>
        </li>
        <li class="section-topics--navi-item green fadeup">
          <a class="link" href="">
            <div class="_icon">
              <img src="<?php echo $PATH;?>/assets/images/top/icon-navi02.svg" alt="">
            </div>
            <div class="_infor">
              アート活動を発信したい！
              <span>アーティスト</span>
            </div>
          </a>
        </li>
        <li class="section-topics--navi-item pink fadeup">
          <a class="link" href="">
            <div class="_icon">
              <img src="<?php echo $PATH;?>/assets/images/top/icon-navi03.svg" alt="">
            </div>
            <div class="_infor">
              アーティストを支えたい！
              <span>支援者</span>
            </div>
          </a>
        </li>
      </ul><!-- ./section-topics--navi-list -->

      <div class="section-topics--navi-direct fadeup">
        <p class="section-topics--navi-direct-ttl"><span>みんなで創ろう、育てよう。 </span></p>
        <div class="section-topics--navi-direct-link align-center">
          <a href="" class="btn-white link"><span>投稿方法を詳しく見る</span></a>
        </div>
      </div><!-- ./section-topics--navi-direct -->

    </div><!-- ./section-topics--navi -->
  </section><!-- ./section-topics -->

  <section class="section-column">
    <h2 class="section-ttl fadeup">
      <span>COLUMNS</span>
      コラム
    </h2>
    <div class="section-column--inner">
      <div class="container3">
        <ul class="section-column--list">
          <li class="section-column--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column">いまこそ語ろう、演劇のこと</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <span class="label-column new">NEW</span>
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <p class="number">No.004</p>
                  <h4 class="title-lv4">劇団チョコレートケーキ（後編）</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">読む</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column yellow">アーティスト・サバイバル・メソッド</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <p class="number">No.004</p>
                  <h4 class="title-lv4">アーティストの仕事図鑑 #1　滝戸ドリタ</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">読む</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column blue">東京の静寂を探しに</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column03.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <p class="number">No.004</p>
                  <h4 class="title-lv4">小石川後楽園＜後編＞</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">読む</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column red">アーティスト解体新書</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column04.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <p class="number">No.004</p>
                  <h4 class="title-lv4">ラファエロ・サンティ</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">読む</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column blue2">イベント・レポート</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <p class="number">アーティスト・サバイバル・メソッド</p>
                  <h4 class="title-lv4">伝説のアパートを「記憶」をもとに再現「豊島区立トキワ荘マンガミュージアム」</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">読む</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column purple">もう一度読みたい！人気コラム</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <span class="label-column purple">Pick up</span>
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <p class="number">No.004</p>
                  <h4 class="title-lv4">小石川後楽園＜後編＞</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">読む</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
        </ul>
        <div class="align-center fadeup">
          <a href="" class="view-more2"><span>コラム一覧へ</span></a>
        </div>
      </div>
    </div>
  </section><!-- ./section-column -->

  <section class="section-search">
    <h2 class="section-ttl fadeup">
      <span>EVENTS</span>
      展覧会・イベント情報
    </h2>
    <div class="section-search--inner">
      <div class="container3">
        <ul class="section-search--list">
          <li class="section-search--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column blue">まもなく開催</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search01.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column blue">まもなく開催</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">特別展「国立ベルリン・エジプト博物館所蔵　古代エジプト展　天地創造の神話」<span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">The Artcomplex Center of Tokyo／アートコンプレックスセンター</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column blue">まもなく開催</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search02.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column blue">まもなく開催</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">佐々木琢磨 15周年記念絵画展ツアー2021,2022 ALL WE ARE <span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">並樹画廊</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column green">開催中</span>
                <span class="label-column purple">オンライン</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search03.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column green">開催中</span>
                    <span class="label-column purple">オンライン</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">NAKED QUILT販売会<span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">Room_412</p>
                </div>
              </div>
            </a>
          </li>
          <li class="section-search--item fadeup">
            <a href="" class="img-hover-zoomWrap">
              <div class="section-search--item-label sp-only3">
                <span class="label-column">NEW</span>
                <span class="label-column green">開催中</span>
                <span class="label-column purple">オンライン</span>
                <span class="label-column white">歴史・民俗(博物館)</span>
              </div>
              <div class="section-search--item-inforWrap">
                <div class="section-search--item-thumb img-hover-zoom">
                  <img src="<?php echo $PATH;?>/assets/images/top/search04.png" alt="">
                </div>
                <div class="section-search--item-infor">
                  <div class="section-search--item-label pc-only3">
                    <span class="label-column">NEW</span>
                    <span class="label-column green">開催中</span>
                    <span class="label-column purple">オンライン</span>
                    <span class="label-column white">歴史・民俗(博物館)</span>
                  </div>
                  <h3 class="ttl-bold">NAKED QUILT販売会<span class="icon-blank"></span></h3>
                  <p class="date">2021.04.03. - 2022.04.03.</p>
                  <p class="section-search--item-msg-ttl">Room_412</p>
                </div>
              </div>
            </a>
          </li>
        </ul>
        <div class="align-center fadeup">
          <a href="" class="view-more2"><span>展覧会・イベント情報へ</span></a>
        </div>
      </div>
    </div>
  </section><!-- ./section-search -->

  <section class="section-artists">
    <h2 class="section-ttl fadeup">
      <span>TOKYO ARTISTS</span>
      トーキョー・アーティスト
    </h2>
    <div class="section-artists--inner">
      <div class="container3">
        <div class="section-artists--listWrap">
          <div class="pagingInfo js-pagingInfo"></div>
          <ul class="section-artists--list slick2 js-slider-artists">
            <li class="section-artists--item fadeup">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists01.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists02.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists03.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists04.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists05.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists06.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists03.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
          </ul>
          <div class="section-artists--direct fadein">
            <a href="" class="link" target="_blank">
              <div class="section-artists--direct-top">
                <div class="section-artists--direct-img">
                  <img src="<?php echo $PATH;?>/assets/images/top/icon-insta-white.svg" alt="">
                </div>
                <p class="section-artists--direct-txt">あなたの作品や活動を<br class="pc-only3">発信してみませんか？</p>
              </div>
              <span class="section-artists--direct-link">詳しくはこちら</span>
            </a>
      
          </div>
        </div>
        <div class="align-center fadeup">
          <a href="" class="view-more2"><span>アーティスト一覧へ</span></a>
        </div>
      </div>
    </div>
  </section><!-- ./section-artists -->

  <section class="section-contest">
    <h2 class="section-ttl fadeup">
      <span>CONTESTS</span>
      コンテスト情報
    </h2>
    <div class="section-contest--inner">
      <div class="container3">
        <ul class="section-contest--list">
          <li class="section-contest--item fadeup">
            <a href="" class="img-hover-zoomWrap" target="_blank">
              <div class="section-contest--item-inner">
                <span class="label-column new">NEW</span>
                <div class="section-contest--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/contest01.png" alt="">
                </div>
                <div class="section-contest--item-infor">
                  <h3 class="ttl-bold en">DESIGN INTELLIGENCE AWARD 2021</h3>
                  <p class="section-contest--item-des">中国美術学院（China Academy of Art）</p>
                </div>
              </div>
              <div class="section-contest--item-direct">
                <span>公式サイトへ</span>
              </div>
            </a>
          </li>
          <li class="section-contest--item fadeup">
            <a href="" class="img-hover-zoomWrap" target="_blank">
              <div class="section-contest--item-inner">
                <span class="label-column green">受付中</span>
                <div class="section-contest--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/contest02.png" alt="">
                </div>
                <div class="section-contest--item-infor">
                  <h3 class="ttl-bold en">佐田岬ワンダービューコンペティション 2021-22</h3>
                  <p class="section-contest--item-des">佐田岬ワンダービューコンペティション実行委員会（伊方町役場 観光商工課内）</p>
                </div>
              </div>
              <div class="section-contest--item-direct">
                <span>公式サイトへ</span>
              </div>
            </a>
          </li>
          <li class="section-contest--item fadeup">
            <a href="" class="img-hover-zoomWrap" target="_blank">
              <div class="section-contest--item-inner">
                <span class="label-column gray">まもなく終了</span>
                <div class="section-contest--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/contest03.png" alt="">
                </div>
                <div class="section-contest--item-infor">
                  <h3 class="ttl-bold en">TOKYO MIDTOWN AWARD 2021 デザインコンペ</h3>
                  <p class="section-contest--item-des">東京ミッドタウン</p>
                </div>
              </div>
              <div class="section-contest--item-direct">
                <span>公式サイトへ</span>
              </div>
            </a>
          </li>
          <li class="section-contest--item fadeup">
            <a href="" class="img-hover-zoomWrap" target="_blank">
              <div class="section-contest--item-inner">
                <span class="label-column gray">まもなく終了</span>
                <div class="section-contest--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/contest03.png" alt="">
                </div>
                <div class="section-contest--item-infor">
                  <h3 class="ttl-bold en">TOKYO MIDTOWN AWARD 2021 デザインコンペ</h3>
                  <p class="section-contest--item-des">東京ミッドタウン</p>
                </div>
              </div>
              <div class="section-contest--item-direct">
                <span>公式サイトへ</span>
              </div>
            </a>
          </li>
        </ul><!-- ./section-contest--list -->
        <div class="align-center fadeup">
          <a href="" class="view-more2"><span>アーティスト一覧へ</span></a>
        </div>
      </div>
    </div>
  </section><!-- ./section-contest -->

  <section class="section-support">
    <h2 class="section-ttl fadeup">
      <span>SUPPORTS</span>
      支援情報
    </h2>
    <div class="section-support--inner">
      <div class="container4">
        <ul class="section-support--list fadeup">
          <li class="section-support--item">
            <a href="" class="link ttl-ani-underlineWrap">
              <div class="section-support--item-inner">
                <div class="section-support--item-label">
                  <span class="label-column">NEW</span>
                  <span class="label-column pink3">通年開催</span>
                  <span class="label-column green">受付中</span>
                </div>
                <h3 class="ttl-bold ttl-ani-underline">芸術文化活動支援事業「アートにエールを！東京プロジェクト（ステージ型）」</h3>
                <p class="desc2">東京都、公益財団法人東京都歴史文化財団</p>
                <div class="section-support--item-tags">
                  <span class="tag">美術</span>
                  <span class="tag">音楽</span>
                  <span class="tag">パフォーマンス</span>
                  <span class="tag">その他</span>
                </div>
                <p class="date">2021.04.03 - 2022.04.03</p>
              </div>
            </a>
          </li>
          <li class="section-support--item">
            <a href="" class="link ttl-ani-underlineWrap">
              <div class="section-support--item-inner">
                <div class="section-support--item-label">
                  <span class="label-column">NEW</span>
                  <span class="label-column pink3">通年開催</span>
                  <span class="label-column green">受付中</span>
                </div>
                <h3 class="ttl-bold ttl-ani-underline">芸術文化活動支援事業「アートにエールを！東京プロジェクト（ステージ型）」</h3>
                <p class="desc2">東京都、公益財団法人東京都歴史文化財団</p>
                <div class="section-support--item-tags">
                  <span class="tag">美術</span>
                  <span class="tag">音楽</span>
                  <span class="tag">パフォーマンス</span>
                  <span class="tag">その他</span>
                </div>
                <p class="date">2021.04.03 - 2022.04.03</p>
              </div>
            </a>
          </li>
        </ul>
        <div class="align-center fadeup">
          <a href="" class="view-more2"><span>支援情報一覧へ</span></a>
        </div>
      </div>
    </div>
  </section><!-- ./section-support -->

  <div class="section-sns fadeup">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->


</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>