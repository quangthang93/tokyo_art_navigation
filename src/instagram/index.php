<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-2.php'; ?>
<main class="main">
  <section class="p-end--banner">
    <div class="container5 align-center">
      <h1 class="p-end--ttl3">TOKYO ARTISTS</h1>
      <p class="p-end--ttlJP">トーキョーアーティスト</p>
      <p class="title-lv5">あなたの作品や活動をTokyoから発信しよう。<br> 公式アカウントをフォロー＋ハッシュタグをつけて投稿で参加できます。</p>
    </div>
    <div class="p-insta--tagWrap">
      <div class="p-insta--tagWrap-inner">
        <p class="p-insta--tagWrap-label">参加ハッシュタグ</p>
        <p class="p-insta--tagWrap-tag en"><span>#tantokyoartist</span></p>
      </div>
    </div>
    <div class="p-insta--icon">
      <p class="p-insta--icon-ttl"><span>FOLLOW US !!</span></p>
      <div class="p-insta--icon-img"></div>
      <div class="p-insta--tagWrap"><span class="p-insta--tag">@Tokyo Art Navigation</span></div>
    </div>
    <p class="desc2 align-center mgt-20">※投稿された写真は＠Tokyo Art Navigationにて、<br class="sp-only3">リポスト投稿で紹介する場合があります。</p>
  </section><!-- ./p-recruit--banner -->
  <div class="p-end--cnt p-insta mgb-80">
    <div class="container5">
      <div class="p-insta--cnt">
        <div class="tabs">
          <div class="tabs-navWrapper">
            <ul class="tabs-nav js-tabsNav">
              <li class="tabs-item js-tabsItem active">ALL</li>
              <li class="tabs-item js-tabsItem">
                <div>ARTWORK<span>#tanartwork</span></div>
              </li>
              <li class="tabs-item js-tabsItem">
                <div>PHOTO / MOVIE<span>#tanphoto</span></div>
              </li>
              <li class="tabs-item js-tabsItem">
                <div>PERFORMANCE<span>#tanperform</span></div>
              </li>
            </ul>
          </div>
          <p class="p-insta--cnt-intro">
          <span class="txt-bold">投稿された画像からお気に入りのアーティストを探してみませんか</span> &nbsp;<br class="sp-only3">
          ※イメージをクリックすると、instagramに移動します</p>
          <div class="tabs-cnt js-tabsCnt">
            <div class="tabs-panel js-tabsPanel">
              <ul class="js-listInsta col4-2">
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists01.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists02.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists03.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists07.jpg" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists05.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists01.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists02.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists03.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists04.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists05.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
              </ul>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <ul class="js-listInsta col4-2">
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists01.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists02.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
              </ul>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <ul class="js-listInsta col4-2">
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists01.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
              </ul>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <ul class="js-listInsta col4-2">
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists01.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
                <li class="col4-2-item">
                  <a href="" class="img-hover-zoomWrap">
                    <div class="section-artists--item-thumb img-hover-zoom">
                      <img class="js-instaImg cover" src="<?php echo $PATH;?>/assets/images/top/artists02.png" alt="">
                    </div>
                    <p class="section-artists--item-ttl">@ Tamabi_member</p>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="view-moreWrap align-center">
            <a href="javascript:void(0)" class="view-more js-btnViewmore">もっと見る</a>
            <img class="js-loading" src="<?php echo $PATH;?>/assets/images/common/icon-download.gif" alt="">
          </div>
          <div class="p-insta--direct">
            <a class="p-insta--direct-link link" href="" target="_blank">#tantokyoartistの最新の投稿はこちら</a>
          </div>
        </div>
      </div>
      <div class="p-artist--directs">
        <div class="p-artist--directs-cnt">
          <p class="ttl-bold">アーティストの方へ</p>
          <p class="desc2">あなたの作品や活動をTokyoから発信してみませんか？ TANの公式アカウントをフォローして、ハッシュタグをつけて投稿してください。東京都内で創作活動されているアーティストなら、どなたでも参加できます。</p>
        </div>
        <div class="p-artist--directs-link">
          <a class="view-more" href="">投稿方法を見る</a>
        </div>
      </div>
      <div class="p-insta--cnt-notices">
        <div class="p-end--ttlIntroWrap">
          <p class="p-end--ttlIntro" href=""><span>ご注意事項</span></p>
        </div>
        <div class="p-insta--cnt-notices-list">
          <div class="p-insta--cnt-notices-item">
            <p class="p-insta--cnt-notices-item-ttl">●掲載に当たっては、運営事務局の掲載基準に基づき、掲載の可否を判断しております。掲載可否の内容は以下の通りです。 ※掲載可否基準は、順次変更していく場合があります。詳しくは「ご利用規約」をご確認ください。</p>
            <ul class="p-insta--cnt-notices-item-subList">
              <li>公序良俗に反するもの、その恐れがあるもの</li>
              <li>アダルト関連のもの、またはその恐れがあるもの</li>
              <li>犯罪行為を誘発する恐れのあるもの</li>
              <li>青少年に悪影響を及ぼす恐れがあるもの</li>
              <li>第三者の著作権・商標権等の知的財産を侵害している内容、またその恐れがあるもの</li>
              <li>第三者の名誉・信用・プライバシー・肖像権等の人格的権利を侵害している内容、若しくはその恐れがあるもの。差別をするもの若しくはそれを助長するもの。</li>
              <li>法律・法規・条例等に反する広告、またはその恐れがあるもの</li>
              <li>反社会的なもの</li>
              <li>虚偽・誇大ならびに事実誤認を生じさせるものがある内容</li>
              <li>その他、弊社確認により掲載不可と判断したもの</li>
            </ul>
          </div>
          <div class="p-insta--cnt-notices-item">
            <p class="p-insta--cnt-notices-item-ttl">●掲載後におきましても、ユーザークレームの発生、またはその危惧があると判断したものは、掲載途中であっても、掲載を停止する場合がございますのでご了承ください。（その理由説明の義務は負わないものとさせていただきます）</p>
          </div>
          <div class="p-insta--cnt-notices-item">
            <p class="p-insta--cnt-notices-item-ttl">●掲載にあたっては、アーティストとしての活動歴について、公表されている方に限らせていただきます。</p>
          </div>
          <div class="p-insta--cnt-notices-item">
            <p class="p-insta--cnt-notices-item-ttl">●掲載にあたって、ご投稿内容の確認を行い、承認された投稿のみを掲載しております。</p>
          </div>
          <div class="p-insta--cnt-notices-item">
            <p class="p-insta--cnt-notices-item-ttl">●ご投稿から掲載までにお時間がかかる場合がございますので、予めご了承ください。</p>
          </div>
          <div class="p-insta--cnt-notices-item">
            <p class="p-insta--cnt-notices-item-ttl">●掲載するご投稿は1人1点までとさせていただきます。</p>
          </div>
        </div>
      </div>
      <div class="align-center mgt-60 fadeup delay-2">
        <a href="" class="view-more2"><span>トップページへ</span></a>
      </div>
    </div>
  </div><!-- ./p-end--cnt -->
  <div class="section-sns fadeup delay-2">
    <p class="section-sns--ttl"><span>公式アカウントをフォローして<br> 東京のアートの”イマ”に触れよう！ </span></p>
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-facebook.svg" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->
</main><!-- ./main -->
<div class="breadcrumbWrap">
  <div class="container">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>トーキョーアーティスト</li>
      </ul>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>