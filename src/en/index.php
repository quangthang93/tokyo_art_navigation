<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-en.php'; ?>
<main class="main">
  <div class="main-mv">
    <div class="container fadein">
      <ul class="main-mv--slider js-slider-top">
        <li class="main-mv--slider-item">
          <a href="" class="main-mv--slider-itemInner link">
            <div class="main-mv--slider-thumbWrap">
              <div class="main-mv--slider-thumb">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/top/mv-slider01.png" alt="">
              </div>
            </div>
            <div class="main-mv--slider-cnt">
              <div class="main-mv--slider-cntInner">
                <div class="main-mv--slider-label">
                  <span class="label">Event Report</span>
                </div>
                <p class="ttl-bold">Let’s play with children and adults to the fullest!</p>
                <h3 class="main-mv--slider-ttl ttl-blue"><span>PLAY! MUSEUM PLAY! PARK</span></h3>
                <p class="desc">The slogan is “encounter with the unknown.” Enjoy physical play and workshops in 7 areas such as the white and soft <Large Plate> and <Factory>.</p>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">Read more</span>
                </div>
              </div>
            </div>
          </a>
        </li>
        <li class="main-mv--slider-item">
          <a href="" class="main-mv--slider-itemInner link">
            <div class="main-mv--slider-thumbWrap">
              <div class="main-mv--slider-thumb">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/top/mv-slider01.png" alt="">
              </div>
            </div>
            <div class="main-mv--slider-cnt">
              <div class="main-mv--slider-cntInner">
                <div class="main-mv--slider-label">
                  <span class="label">Event Report</span>
                </div>
                <p class="ttl-bold">Let’s play with children and adults to the fullest!</p>
                <h3 class="main-mv--slider-ttl ttl-blue"><span>PLAY! MUSEUM PLAY! PARK</span></h3>
                <p class="desc">The slogan is “encounter with the unknown.” Enjoy physical play and workshops in 7 areas such as the white and soft <Large Plate> and <Factory>.</p>
                <div class="main-mv--slider-dateWrap">
                  <span class="date">2021.04.21</span>
                  <span class="view-more">Read more</span>
                </div>
              </div>
            </div>
          </a>
        </li>
      </ul><!-- ./main-mv--slider -->
      <div class="main-mv--news">
        <p class="main-mv--news-ttl">NEWS</p>
        <a href="" class="main-mv--news-cnt link">
          <p class="date">2021.04.25</p>
          <p class="desc">Notice of system maintenance</p>
        </a>
      </div>
      <p class="main-mv--notices">Each event may be canceled or changed to prevent the spread of new coronavirus infection. Please check the official website of each event for the latest information.</p>
    </div><!-- ./container -->
  </div><!-- ./main-mv -->

  <section class="section-msg">
    <div class="section-msg--cnt">
      <p class="desc"><span class="txt-bold">Tokyo Art Navigation</span> is a website that was established with the aim of bringing art closer to the citizens of Tokyo by introducing the current culture and art scene in Tokyo. There are many exhibitions and concerts being held in the art museums and music halls of Tokyo. The Tokyo Art Navigation site introduces information on these activities in real time.</p>
      <p class="desc">In addition, in an effort to support the activities of artists, the site also provides information as to activity spaces, available grants-in-aid, as well as contests. It is expected that the site will be fully utilized as a venue from which artists engaged in creative activities will be able to distribute their work and activity information.</p>
      <p class="desc">(This site is operated by the Tokyo Metropolitan Foundation for History and Culture through a subsidy from the Tokyo Metropolitan Government.)</p>
    </div>
  </section>

  <section class="section-column">
    <h2 class="section-ttl fadeup">
      <span>COLUMN</span>
    </h2>
    <div class="section-column--inner">
      <div class="container3">
        <ul class="section-column--list">
          <li class="section-column--item fadeup delay-2">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column">Now is the time to talk about theater</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <span class="label-column new">NEW</span>
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <h4 class="title-lv4">Theatrical company chocolate cake (second part)</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">Read more</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup delay-4">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column yellow">Artist Survival Method</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <h4 class="title-lv4">Artist’s work picture book # 1 Dorita Takito</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">Read more</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup delay-6">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column blue">Looking for the silence of Tokyo</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column03.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <h4 class="title-lv4">Koishikawa Korakuen <Part 2></h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">Read more</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup delay-2">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column red">Examining artists</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column04.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <p class="number">No.004</p>
                  <h4 class="title-lv4">Raphael Santi</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">Read more</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup delay-4">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column blue2">Event report</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <h4 class="title-lv4">Recreating a Legendary Apartment from Memories<br> Toshima City Tokiwaso Manga Museum</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">Read more</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
          <li class="section-column--item fadeup delay-6">
            <a href="" class="img-hover-zoomWrap">
              <h3 class="ttl-column purple">I want to read it again! Popular column</h3>
              <div class="section-column--inforWrap">
                <div class="section-column--item-thumbWrap">
                  <span class="label-column purple">Pick up</span>
                  <div class="img-hover-zoom">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/top/column05.png" alt="">
                  </div>
                </div>
                <div class="section-column--infor">
                  <h4 class="title-lv4">Apply for the contest [Part 1]</h4>
                  <div class="main-mv--slider-dateWrap">
                    <span class="date">2021.04.21</span>
                    <span class="view-more">Read more</span>
                  </div>
                </div>
              </div>
            </a>
          </li>
        </ul>
        <div class="align-center fadeup">
          <a href="" class="view-more2"><span>VIEW ALL</span></a>
        </div>
      </div>
    </div>
  </section><!-- ./section-column -->

  <section class="section-artists">
    <h2 class="section-ttl type2 fadeup">
      <span>TOKYO ARTISTS</span>
    </h2>
    <div class="section-artists--inner">
      <div class="container3">
        <div class="section-artists--listWrap">
          <div class="pagingInfo js-pagingInfo"></div>
          <ul class="section-artists--list slick2 js-slider-artists">
            <li class="section-artists--item fadeup delay-2">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists01.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup delay-3">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists02.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup delay-4">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists03.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup delay-5">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists04.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup delay-6">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists05.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup delay-7">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists06.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
            <li class="section-artists--item fadeup delay-8">
              <a href="" class="img-hover-zoomWrap">
                <div class="section-artists--item-thumb img-hover-zoom">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/top/artists03.png" alt="">
                </div>
                <p class="section-artists--item-ttl">@ Tamabi_member</p>
              </a>
            </li>
          </ul>
          <div class="section-artists--direct fadein">
            <a href="" class="link" target="_blank">
              <div class="section-artists--direct-top">
                <div class="section-artists--direct-img">
                  <img src="<?php echo $PATH;?>/assets/images/top/icon-insta-white-en.svg" alt="">
                </div>
                <p class="section-artists--direct-txt">Introducing<br class="pc-only3"> artists who are <br class="pc-only3">active in Tokyo</p>
              </div>
            </a>
      
          </div>
        </div>
        <div class="align-center fadeup delay-2">
          <a href="" class="view-more2"><span>VIEW ALL</span></a>
        </div>
      </div>
    </div>
  </section><!-- ./section-artists -->

  <section class="section-collection">
    <div class="container3">
      <div class="section-collection--inner">
        <div class="section-collection--thumb">
          <img src="<?php echo $PATH;?>/assets/images/top/collection.png" alt="">
        </div>
        <div class="section-collection--cnt">
          <h3 class="section-collection--ttl">Tokyo Museum Collection</h3>
          <p class="ttl-bold2">Launch of Tokyo Museum Collection: Integrated Database Search for Metropolitan Museums</p>
          <p class="desc">Tokyo Museum Collection: Integrated Database Search for Metropolitan Museums, which enables cross-search of materials and artworks held by six metropolitan museums (Edo-Tokyo Museum, Edo-Tokyo Open-Air Architectural Museum, Museum of Contemporary Art Tokyo, Tokyo Photographic Art Museum, Tokyo Metropolitan Teien Art Museum, and Tokyo Metropolitan Art Museum).</p>
          <div>
            <a href="" class="link-icon2 blank txt-bold">Tokyo Museum Collection</a>
          </div>
        </div>
      </div>
    </div>
  </section><!-- ./section-collection -->

  <div class="section-sns fadeup delay-2">
    <div class="section-sns--link">
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.png" alt="">
      </a>
      <a href="" class="link" target="_blank">
        <img class="cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
      </a>
    </div>
  </div><!-- ./section-sns- -->


</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-en.php'; ?>